function [xReturn,error,msg] = getUnitBOM(obj,workorder)
%getUnitBOM(obj,workorder)
%
%   Returns BOM components for product associated with specified work order 
%   
%     Input:
%       workorder = (string)
%   
%     Output:
%       return = (unitBOM)
%       error = (int)
%       msg = (string)

% Build up the argument lists.
values = { ...
   workorder, ...
   };
names = { ...
   'workorder', ...
   };
types = { ...
   '{http://www.w3.org/2001/XMLSchema}string', ...
   };

% Create the message, make the call, and convert the response into a variable.
soapMessage = createSoapMessage( ...
    'urn:runcard_wsdl', ...
    'getUnitBOM', ...
    values,names,types,'rpc');
response = callSoapService( ...
    obj.endpoint, ...
    'urn:runcard_wsdl#getUnitBOM', ...
    soapMessage);
[xReturn,error,msg] = parseSoapResponse(response);
