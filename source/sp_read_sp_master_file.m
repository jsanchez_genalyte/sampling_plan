function  [ sampling_plan,sp_names_cel,  error_message ]  = sp_read_sp_master_file(sp_master_excel_file_name)
%SP_READ_SP_MASTER_FILE   reads the sampling plan master file and returns a list with the columns.
% Each column represents a sampling plan. 
% The sp_master_excel_file_name contains the full path to the file.
% Nick is the owner of the file, and He will periodically updates the file as needed. 

% JAS_Temporal: Rather than reading the file, right now I am hardcoding the first 4 sampling plans. jas_tbd.
% Sample call: 
% [ sampling_plan,  error_message ]  = sp_read_sp_master_file('C:\Users\jsanchez\Documents\smpling_plan\configure\sampling_plan_master.xlsm');
% pipeline:    sampling_plan  --> sp_part_bin_update  -->  sp_read_sp_master_file  -->  sp_get_excel_range
fprintf('\n Master Sampling plan File ');
%fprintf('\n                             = %-s\n',sp_master_excel_file_name);

[ sampling_plan, sp_names_cel, error_message ] = sp_get_excel_range( sp_master_excel_file_name,1);
if ((isempty(error_message)) || (strcmp(error_message(1:7),'WARNING')) )
    %fprintf('\n DONE  ... reading Sampling Plans: \t   %-3d sampling plans\n',size(sampling_plan,2));
    fprintf('\n Sampling Plans read count   = %-3d ',size(sampling_plan,2));      
    fprintf('\n Reading Sampling Plans    ... DONE           ');        
    return;
end
end % fn sp_read_sp_master_file 
 
% % 
% % 
% % sampling_plan   = {};
% % error_message   = '';
% % % TBD:
% % % 1 - Open File
% % % 2 - Read tab 1
% % % 3 - Close File.
% % % 4 - packl contents into a list. 
% % 
% % sampling_plan{1}= [...
% %     11
% %     49
% %     62
% %     111
% %     132
% %     181
% %     194
% %     232
% %     ];
% % 
% % sampling_plan{2}= [ ...
% %     12
% %     50
% %     63
% %     112
% %     133
% %     182
% %     195
% %     233
% %     ];
% % 
% % sampling_plan{3}= [ ...
% %     1
% %     3
% %     5
% %     7
% %     9
% %     13
% %     15
% %     17
% %     19
% %     21
% %     23
% %     25
% %     27
% %     29
% %     31
% %     33
% %     35
% %     37
% %     39
% %     41
% %     43
% %     45
% %     47
% %     51
% %     53
% %     55
% %     57
% %     59
% %     61
% %     65
% %     67
% %     69
% %     71
% %     73
% %     75
% %     77
% %     79
% %     81
% %     83
% %     85
% %     87
% %     89
% %     91
% %     93
% %     95
% %     97
% %     99
% %     101
% %     103
% %     105
% %     107
% %     109
% %     113
% %     115
% %     117
% %     119
% %     121
% %     123
% %     125
% %     127
% %     129
% %     131
% %     135
% %     137
% %     139
% %     141
% %     143
% %     145
% %     147
% %     149
% %     151
% %     153
% %     155
% %     157
% %     159
% %     161
% %     163
% %     165
% %     167
% %     169
% %     171
% %     173
% %     175
% %     177
% %     179
% %     183
% %     185
% %     187
% %     189
% %     191
% %     193
% %     197
% %     199
% %     201
% %     203
% %     205
% %     207
% %     209
% %     211
% %     213
% %     215
% %     217
% %     219
% %     221
% %     223
% %     225
% %     227
% %     229
% %     231
% %     235
% %     237
% %     239
% %     241
% %     ];
% % 
% % sampling_plan{4}= [ ...
% %     2
% %     4
% %     6
% %     8
% %     10
% %     14
% %     16
% %     18
% %     20
% %     22
% %     24
% %     26
% %     28
% %     30
% %     32
% %     34
% %     36
% %     38
% %     40
% %     42
% %     44
% %     46
% %     48
% %     52
% %     54
% %     56
% %     58
% %     60
% %     64
% %     66
% %     68
% %     70
% %     72
% %     74
% %     76
% %     78
% %     80
% %     82
% %     84
% %     86
% %     88
% %     90
% %     92
% %     94
% %     96
% %     98
% %     100
% %     102
% %     104
% %     106
% %     108
% %     110
% %     114
% %     116
% %     118
% %     120
% %     122
% %     124
% %     126
% %     128
% %     130
% %     134
% %     136
% %     138
% %     140
% %     142
% %     144
% %     146
% %     148
% %     150
% %     152
% %     154
% %     156
% %     158
% %     160
% %     162
% %     164
% %     166
% %     168
% %     170
% %     172
% %     174
% %     176
% %     178
% %     180
% %     184
% %     186
% %     188
% %     190
% %     192
% %     196
% %     198
% %     200
% %     202
% %     204
% %     206
% %     208
% %     210
% %     212
% %     214
% %     216
% %     218
% %     220
% %     222
% %     224
% %     226
% %     228
% %     230
% %     234
% %     236
% %     238
% %     240
% %     242
% %     ];
% % 
% % sampling_plan{11}= [ ...
% % 1
% % 3
% % 5
% % 7
% % 9
% % 12
% % 14
% % 16
% % 18
% % 20
% % 23
% % 25
% % 27
% % 29
% % 32
% % 34
% % 36
% % 38
% % 40
% % 43
% % 45
% % 47
% % 49
% % 52
% % 54
% % 56
% % 58
% % 60
% % 63
% % 65
% % 67
% % 69
% % 72
% % 74
% % 76
% % 78
% % 80
% % 83
% % 85
% % 87
% % 89
% % 92
% % 94
% % 96
% % 98
% % 100
% % 103
% % 105
% % 107
% % 109
% % 112
% % 114
% % 116
% % 118
% % 120
% % 123
% % 125
% % 127
% % 129
% % 131
% % 134
% % 136
% % 138
% % 140
% % 143
% % 145
% % 147
% % 149
% % 151
% % 154
% % 156
% % 158
% % 160
% % 163
% % 165
% % 167
% % 169
% % 171
% % 174
% % 176
% % 178
% % 180
% % 183
% % 185
% % 187
% % 189
% % 191
% % 194
% % 196
% % 198
% % 200
% % 203
% % 205
% % 207
% % 209
% % 211
% % 214
% % 216
% % 218
% % 220
% % 223
% % 225
% % 227
% % 229
% % 231
% % 234
% % 236
% % 238
% % 240
% % ];
% % 
% % sampling_plan{12}= [ ...
% % 2
% % 15
% % 26
% % 39
% % 50
% % 62
% % 75
% % 86
% % 99
% % 110
% % 124
% % 135
% % 148
% % 159
% % 172
% % 184
% % 195
% % 208
% % 219
% % 232
% %    ]; 



