function  [ error_message ]  = sp_part_bin_update(input_params)
%SP_PART_BIN_UPDATE updates the bin for the chips defined for a given sampling plan, a spot lot and list of chips).
%  Version 3.0 Using 11th ... Mario's version (source version 1.0)      MAIN_INPUT / PROCESS / OUTPUT
%  NOTES: Missing items: Find from the previous steps the 244 serials  step==seq_num-10=3-   op_code==?A230
%                        Find serials in current  steps that are not in previous: add ons to replace the missing/failures
%                        Find serials in previous steps that are not in current:  failures (what kind??? Alex)
%                        assemble list with correct order (243,244 in position 20 and 40)
%                        assemble a column: with pass /fail.
%                        add logic for finding nearest neighboor. (see backup_sp_Ver_200 dir)
%
% pipleine:  sp_part_bin_update  --> sp_read_sp_master_file -->
%                               -->  perform_sp -->
% From Ryan:
% Here's the working example for doing part bin updates to tag which chip goes on which sampling plan.
%

% Connect to run card:

%input_params.webservice_url         = 'http://10.0.2.226/runcard_test/soap?wsdl'; % test
%webservice_url                      = 'http://10.0.2.226/runcard/soap?wsdl';      % production

debug_flag = false; % version 
if (debug_flag)
    input_params
end
sampling_plan_number_to_use = input_params.sampling_plan_number_to_use;
sp_master_excel_file_name   = input_params.sp_master_excel_file_name;
wo_number                   = input_params.wo_number;
seq_num                     = input_params.seq_num;
op_code                     = input_params.op_code;
op_code_previous_step       = input_params.op_code_previous_step;
bin_number_from             = input_params.bin_number_from;
bin_number_to               = input_params.bin_number_to;

webservice_url               = input_params.webservice_url;
part_number                  = input_params.part_number;  % Alex: part_number = 'EXP_CHIP'; %  Mario: '29005' 


sp_ok_do_partially_flag     = input_params.sp_ok_do_partially_flag;
replace_chip_if_miss_flag   = input_params.replace_chip_if_miss_flag;
check_availab_only_flag     = input_params.check_availab_only_flag;
logical_str                = {'FALSE', 'TRUE'};

% Real Processing:

% Open SP master file (excel) and read the available sampling plans.
[ sampling_plan, sp_names_cel, error_message ]  = sp_read_sp_master_file(sp_master_excel_file_name); % 'C:\Users\jsanchez\Documents\dcrc\data\sp_master.xlsm');
if (~(isempty(error_message))) 
    return;
end


sampling_plan_cnt = size(sampling_plan,2);
% fprintf('\n Read %4d             sampling plans: %s\n',sampling_plan_cnt);

sampling_plan_number_to_use = double(sampling_plan_number_to_use);
if (sampling_plan_cnt < sampling_plan_number_to_use)
    error_message = sprintf(...
        '\n ERROR: Invalid Sampling Plan Number: %4d. \n There are only %4d available sampling plans in the SAMPLING_PLAN_MASTER file .\n '...
        ,sampling_plan_number_to_use,sampling_plan_cnt);
    return;
else
    sampling_plan_name_to_use       = sp_names_cel{sampling_plan_number_to_use};
end

% CHECK THAT THERE ARE NO BLANKS (Nans in the wanted sampling plan indices)
requested_sp_cnt = size(sampling_plan{1,sampling_plan_number_to_use},1);
blank_ndx = isnan(sampling_plan{1,sampling_plan_number_to_use});
if (sum(blank_ndx))
    % THERE ARE BLANKS ON THE REQUESTED SP: Mistmatch between requested and found indices: Report to user and getout
    last_ok_index = find(blank_ndx,1)-1;
    error_message = sprintf(...
        '\n ERROR: Invalid Sampling Plan Number: %-4d. \n Request cnt is  %-4d and There are only %-4d indices in the SAMPLING PLAN MASTER file .\n '...
        ,sampling_plan_number_to_use,requested_sp_cnt,last_ok_index);
    return;
end

% Connect to run card:
createClassFromWsdl(webservice_url);
obj                                 = runcard_wsdl;                                                   % 'Q410' , 80,??  carrier_id          qty
%                                                                                      A230    30 'GELPACK1LN180046'          2
% Fetch existing:  before spotting:                    %                               D320    40 'GELPACK1LN180046' 1/2/3  240
prev_seq_num = num2str( ((str2double(input_params.seq_num)) - 10));
prev_op_code = op_code_previous_step; % 'A230';
[resp_fvi_spot, error_value, error_message]   = fetchInventoryItems(obj, '','',part_number, '', wo_number, prev_op_code ,  prev_seq_num, '', '');  % op_code=A
if (~(isempty(error_message)))
    error_message =  strcat( error_message, '\nError_value = ',num2str(error_value));
    return
end;
if (isempty(resp_fvi_spot))
    % NO FAILURES
    fail_die_index = nan;
    resp_fvi_spot_table                 = table();
else
    % SOME  FAILURES
    resp_fvi_spot_table                 = struct2table(resp_fvi_spot);
    % Filter out the following conditions:  (part_bin == '1' i.e. bin_number_from)  and  (status == 'IN QUEUE' or 'IN PROGRESS' )
    if (sum(ischar(resp_fvi_spot_table.part_bin)))
        
    end
    resp_fvi_spot_table.part_bin        = categorical(cellstr(resp_fvi_spot_table.part_bin));
    resp_fvi_spot_table.status          = categorical(cellstr(resp_fvi_spot_table.status));
    rows_use_ndx                        = ( (resp_fvi_spot_table.part_bin == bin_number_from) ...         %sp2: for failures: I may  not need to filter: bin_number_from
        & ( (resp_fvi_spot_table.status == 'SCRAPPED') | (resp_fvi_spot_table.status == 'IN PROGRESS') ) ...
        );                        %   get rid of empty records: blank panel name
    %     & (~(resp_fvi_spot_table.rework)) ...      no rework in this step.
    
    % 2018_03_28_rev_108: jas_tbd
    % resp_fvi_spot_table has all bin_number_from so far: We need them all. 
    % for subsequent sampling plans:i.e. sp_1 evens: 2,4,6 ... sp_2: odds: 1,3,5
    % 2 options:
    % 1 - option_1: add an extra_col:  all_bin_bin_number  to all the tables: resp_fvi_spot_table, and all the tables.
    % 1 - option_2: do not trim using rows_use_ndx resp_fvi_spot_table. 
    %     but use column part_number_bin_number ALWAYS to avoid using previously used chips.
    %     i.e. add a clause:  and  part_number_bin_number == bin_number_from EVERYWHERE.    THINK ABOUT BOTH OPTIONS: Wed_Afternoon version_1.08
    % second thing to be done: change the order of the beyond logic so I maintain the order: (per alex request)
    % i.e. start from the end on both sides the sp and the table. and move my way up. 
    
    
    resp_fvi_spot_table                 = resp_fvi_spot_table(rows_use_ndx,:);                          %
    %resp_fvi_spot_cnt                   = height(resp_fvi_spot_table);
    
    fail_serials                        = resp_fvi_spot_table.serial;
    if (isempty(fail_serials))
        fail_die_index = nan;
    else
        delimiter                           = {'.'}; % DOT IS THE ONLY VALID SEPARATOR BETWEEN PARTS OF THE SERIAL.
        fail_die_index_str                  = split(fail_serials,delimiter);
        if (size(fail_die_index_str,1) ==3) && (size(fail_die_index_str,2)==1)
            fail_die_index                  = double(fail_die_index_str(3)); % the last part (the 3rd) is now the die_index
            %fail_die_index_cnt              = size(fail_die_index,2);
        else
            fail_die_index                  = double(fail_die_index_str(:,3)); % the last part (the 3rd) is now the die_index
            %fail_die_index_cnt              = size(fail_die_index,1);
        end
    end
end % Some Failures

% HAVE THE FAILURE DIE_INDEX:
%fetch the existing chip serials, with carrier ID / type and row/col information  40	'D320'	'IN QUEUE'  240 records
%                                                 chip_serial_number              30	'A230'	'SCRAPPED'     2 records
% new parameter:  part_number:  mario:  29005  alex: EXP_CHIP.   in excel.   
% if (strcmpi('EXP_CHIP',part_number))
%     part_number = '';  %jas_temp:tbd with mario.
% end

[resp_avi_spot, error_value, error_message]   = fetchInventoryItems(obj, '','', part_number, '', wo_number, op_code, seq_num, '', ''); %jas_temp+for_mario: '29005', vs ''
if (0)
    error_message =''; %jas_temp_to_be_removed: limit 1k ask Ryan.
    error_value =0;
end
if (debug_flag)
    fprintf('Error_value = %s   Error Message = %s ',num2str(error_value),error_message);
end

if (isempty(resp_avi_spot))
    fprintf('\n Found No Data for resp_avi_spot \n');
else
    if (debug_flag)
        fprintf('\n Found Data size = %4d\n',size(resp_avi_spot,1));
    end
end
if (  (~(isempty(error_message))) || (isempty(resp_avi_spot)) )
    if (isempty(resp_avi_spot))
        error_message =  strcat( error_message, '\nError_value = ',num2str(error_value),' WO die not found');
    else
        error_message =  strcat( error_message, '\nError_value = ',num2str(error_value));
    end
    return;
end;
% from the response filter out only what is 'in queue' or ' in progres' jas_tbd   (note: serial is the dieindex)
% and filter for part_bin == 1 jas_tbd
if error_value ~= 0
    disp(['RunCard exception encountered (fetchInventoryItems): ' error_message]);
    disp( 'Sampling plan could not be executed. Contact Jorge Sanchez ');
    return;
end
% OK FETCH: Compare each chip with the sampling plan: if found: assign the new bin. else: do nothing
% grab the serial.
% from the serial: After the dot is the die_index.
% sort by die index: ascending. % jas_ Do I need to sort them? or always come in ascending order? Mario? Ryan?
% Move the 11th, 49th etc: these come from the sampling plan requested.
% resp_avi_spot_table.serial	    partnum	    partrev	    part_class	    lotnum	    qty	    operator	    rework	    workorder	    seqnum	    opcode	    status	    factory	    warehouseloc	    carrier_type	    carrier_id	    carrier_row	    carrier_col	    warehousebin	    comment	    moddate
if (isempty(resp_avi_spot))
    return;
end
resp_avi_spot_table                 = struct2table(resp_avi_spot);
if (debug_flag)
    fprintf('\n\n Before Filter\n\n')
    resp_avi_spot_table
end

% Filter out the following conditions:  (part_bin == '1' i.e. bin_number_from)  and  (status == 'IN QUEUE' or 'IN PROGRESS' ) % 'SCRAPPED'
resp_avi_spot_table.part_bin        = categorical(resp_avi_spot_table.part_bin);
resp_avi_spot_table.status          = categorical(resp_avi_spot_table.status);
rows_use_ndx                        = (  ... % (resp_avi_spot_table.part_bin == bin_number_from)  &  ...    %sp2: for available: I may  not need to filter: bin_number_from HERE
   ( (resp_avi_spot_table.status == 'IN QUEUE') | (resp_avi_spot_table.status == 'IN PROGRESS') ) ); 
resp_avi_spot_table                 = resp_avi_spot_table(rows_use_ndx,:);                          %
%resp_avi_spot_cnt                   = height(resp_avi_spot_table);

if (debug_flag)
    fprintf('\n\n AFTER Filter\n\n')
    resp_avi_spot_table
end

if (isempty(resp_avi_spot_table))
    available_die_index = nan;
    fprintf('\n\n NO DIE TO WORK WITH AFTER FILTERING for part bin_number from = %-s\n',bin_number_from);
else
    available_serials                   = resp_avi_spot_table.serial;
    delimiter                           = {'.'}; % DOT IS THE ONLY VALID SEPARATOR BETWEEN PARTS OF THE SERIAL.
    available_die_index_str             = split(available_serials,delimiter);
    available_die_index                 = double(available_die_index_str(:,3)); % the last part (the 3rd) is now the die_index
    if (debug_flag)
        fprintf('\n\n OK: available_die_index \n');
    end
end

sp_die_ndx                          = sampling_plan{sampling_plan_number_to_use};
 
% HAVE THE AVAILABLE DIE_INDEX:
% Determine from the available dies, the ones requested from the sampling plan: Check with Mario: Is this correct?



% Perform the selected SP given the available and failure die indexes.                                               % ***********  ALGORITHM *****************                                                                    % ****** %
[ sp_analysis, sp_analysis_bey,error_message ]    = perform_sp( resp_avi_spot_table,resp_fvi_spot_table,available_die_index,fail_die_index,sp_die_ndx,input_params );
if(~(isempty(error_message)))
    fprintf('\n\nTRYING TO PERFORM SAMPLING PLAN: ...\n%s\n\n',error_message);
end

% flags: 1_ sp_ok_do_partially_flag 2_ replace_if_chip_is_miss_flag 3_ check_availab_only_flag

% AT THIS POINT WE HAVE evaluated what can be done:
% 1 - sp_complete_flag:             flag to tell  whether the sp could be completed or not:
% 2 - die_index_to_use:             what is trully available to use
% 3 - die_index_to_use_cnt          cnt of die that could     be used for the requested sp
% 4 - die_index_missing_use_cnt     cnt of die that could not be used for the requested sp
% 5 - die_index_missing_ndx         indices    that could not be used for the requested sp

% Report to user what was found for the requested sp:
% Print Summary of findings:
fprintf('\n WORK ORDER CHIP SUMMARY:');
fprintf('\n    Total into spotting      chip count       %-5d',sp_analysis.resp_all_spot_cnt);
fprintf('\n    Available good           chip count       %-5d',sp_analysis.resp_avi_spot_cnt);
fprintf('\n    Failures (Visual Insp)   chip count       %-5d',sp_analysis.resp_fvi_spot_cnt);
fprintf('\n');
fprintf('\n SAMPLING PLAN REQUEST FLAGS SUMMARY:');
fprintf('\n    Sampling_plan_ok_do_partial               %-s',logical_str{sp_ok_do_partially_flag+1});      % 9  This will enable to do partial sampling plans.
%                                                                                                          %    If set to false: it will not do the sampling plan, unless it can be done completelly
fprintf('\n    Replace chip if is missing                %-s',logical_str{replace_chip_if_miss_flag+1});    % 10 If set to true:  it will do as many as possible.                                                                                                               
fprintf('\n    Check Availability Only (just check)      %-s',logical_str{check_availab_only_flag+1});      % 11 Recomended to see what is going to happening, without doing ANYTHING.
fprintf('\n');
fprintf('\n SAMPLING PLAN DETAILS');
fprintf('\n   Sampling plan name   to use:               %-s' ,sampling_plan_name_to_use);% 1
fprintf('\n   Sampling plan number to use:               %-3d',sampling_plan_number_to_use);% 1
fprintf('\n   Requested            SP chip count         %-5d',sp_analysis.die_index_sp_ndx_cnt);
fprintf('\n');
fprintf('\n SAMPLING PLAN SUMMARY RESULTS');
fprintf('\n Sampling plan can be done completely:        %-s' ,logical_str{sp_analysis.sp_complete_flag+1});
fprintf('\n   Filled   WORK ORDER SP chip count          %-5d',sp_analysis.sp_filled_die_cnt);
fprintf('\n   Missing  WORK ORDER SP chip count          %-5d',sp_analysis.sp_mis_die_cnt);
fprintf('\n');
if (isdeployed)
    % END USERS: Tiny screen
    screen_col_width = 5;
else
    screen_col_width = 10;
end
fprintf('\n   REQUESTED SAMPLING PLAN Die index  \n');
for cur_sp_ndx= 1:1:sp_analysis.die_index_sp_ndx_cnt
    fprintf('\t\t%-4d', sp_analysis.sp_die_ndx(cur_sp_ndx));
    if (mod(cur_sp_ndx,screen_col_width) == 0)
        fprintf('\n');
    end
end
fprintf('\n SAMPLING PLAN DETAIL RESULTS: ___________________________________\n');
fprintf('\n SAMPLING PLAN DETAIL                                        BELOW');
fprintf('\n   Filled  WORK ORDER  SP chip count          %-5d          BELOW' ,sp_analysis.sp_filled_die_cnt-sp_analysis_bey.rpl_from_ok_cnt);
fprintf('\n   Missing WORK ORDER  SP chip count          %-5d          BELOW' ,sp_analysis.sp_mis_die_cnt   -sp_analysis_bey.rpl_from_mis_cnt);

if (sp_analysis.sp_mis_die_cnt)
    fprintf('\n   MISSING Die indices\n');
    for cur_mis_ndx= 1:1:sp_analysis.sp_mis_die_cnt
        if (mod(cur_mis_ndx,screen_col_width) == 0)
            fprintf('\n');
        end
        fprintf('\t\t%-4d', sp_analysis.sp_mis_die_ndx(cur_mis_ndx))
    end
end

fprintf('\n');
if (sp_analysis.resp_fail_cnt)
    fprintf('\n   FAILURE Die index  \t\tSerial\n');
    for cur_fail_ndx= 1:1:sp_analysis.resp_fail_cnt
        fprintf('\n\t\t\t%-4d\t\t\t%-4s', sp_analysis.resp_fail_ndx(cur_fail_ndx), char(sp_analysis.resp_fail_serial(cur_fail_ndx)));
    end
end
fprintf('\n');

if (sp_analysis.other_sp_cnt)
    fprintf('\n   OTHER SP Die index  \tPart_bin_number\t\tSerial\n');
    for cur_other_sp_ndx= 1:1:sp_analysis.other_sp_cnt
        fprintf('\n\t\t\t%-4d\t\t\t%-4s\t\t\t%-4s', sp_analysis.other_sp_ndx(cur_other_sp_ndx), sp_analysis.other_sp_bin(cur_other_sp_ndx), char(sp_analysis.other_sp_serial(cur_other_sp_ndx)));
    end
end
fprintf('\n');


% SUCCESSFUL REPLACEMENTS: I.E. Failures that where replaced with a neighboor
if (sp_analysis.rpl_from_ok_cnt)
    fprintf('\n   REPLACEMENT DIE OK: \n\tindex_fail  \tindex_with   \tserial_fail  \t\tserial_replace');
    for cur_rpl_ndx= 1:1:sp_analysis.rpl_from_ok_cnt
        fprintf('\n\t%-4d\t\t\t%-4d\t\t\t%-s\t\t%-s'...
            , sp_analysis.rpl_from_ok_ndx(cur_rpl_ndx), sp_analysis.rpl_with_ok_ndx(cur_rpl_ndx)...
            , char(sp_analysis.rpl_from_ok_serial(cur_rpl_ndx)), char(sp_analysis.rpl_with_ok_serial(cur_rpl_ndx)));
    end
else
    fprintf('\n   REPLACEMENT DIE: None                                     BELOW');
end
fprintf('\n');
% MISSING REPLACEMENTS: I.E. Failures that where NOT replaced with a neighboor
if (sp_analysis.rpl_from_mis_cnt)
    fprintf('\n   REPLACEMENT DIE MISSING: index_fail  \n');
    for cur_rpl_ndx= 1:1:sp_analysis.rpl_from_mis_ndx
        fprintf('\n\t\t\t\t\t%-4d\t\t%-s'...
            , sp_analysis.rpl_from_mis_ndx(cur_rpl_ndx), char(sp_analysis.rpl_from_mis_serial(cur_rpl_ndx)));
    end
else
    fprintf('\n   REPLACEMENT DIE MISSING: None                             BELOW');    
end

% % true  == Just check if it is possible to do the sp. Do not change any bins.
% % false == Attempt to move the chips to the proper bin. See next flag.
% sp_ok_do_partially_flag    = false; % DEFAULT == false.
% % true  == Perform partial sampling plans and report which part was done and which was not.
% % false == Move the chips to the proper bin: ONLY IFF the sp can be done completely
% replace_if_chip_is_miss_flag= true;  % true  == attempt to find nearest neightboor for missing chips


% Use input flags to decide what to do:
if (input_params.check_availab_only_flag)
    fprintf('\n\n\t******    No Sampling plan was executed!. ***\n\tRequest was just to check for availability. See SIMULATION details...\n');
    %return;
end

% Part 2: Beyond:

if ( ( sp_analysis_bey.rpl_from_ok_cnt >0) || ( sp_analysis_bey.rpl_from_mis_cnt >0) )
    fprintf('\n');
    fprintf('\n SAMPLING PLAN DETAIL                                        BEYOND');
    fprintf('\n   Filled  WORK ORDER  SP chip count          %-5d          BEYOND',sp_analysis_bey.rpl_from_ok_cnt);
    fprintf('\n   Missing WORK ORDER  SP chip count          %-5d          BEYOND',sp_analysis_bey.rpl_from_mis_cnt);
    
    % SUCCESSFUL REPLACEMENTS: I.E. Failures that where replaced with a neighboor
    if (sp_analysis_bey.rpl_from_ok_cnt)
        fprintf(...
        '\n   REPLACEMENT DIE OK:           AVAILABLE\n\tindex_from  \tindex_with   \tserial_from  \t\tserial_replace      BEYOND');
        for cur_rpl_ndx= 1:1:sp_analysis_bey.rpl_from_ok_cnt
            if (isempty(sp_analysis_bey.rpl_from_ok_serial{1}))
                cur_serial_from = '';
            else
                cur_serial_from = char(sp_analysis_bey.rpl_from_ok_serial(cur_rpl_ndx));
            end
            if (isempty(sp_analysis_bey.rpl_with_ok_serial{1}))
                cur_serial_with = '';
            else
                cur_serial_with = char(sp_analysis_bey.rpl_with_ok_serial(cur_rpl_ndx));
            end
            fprintf('\n\t%-4d\t\t\t%-4d\t\t\t%-s\t\t%-s'...
                , sp_die_ndx(sp_analysis_bey.rpl_from_ok_ndx(cur_rpl_ndx)), sp_analysis_bey.rpl_with_ok_ndx(cur_rpl_ndx)...
                , cur_serial_from, cur_serial_with);
        end
        fprintf('\n');
    end
    fprintf('\n');
    % MISSING REPLACEMENTS: I.E. Failures that where NOT replaced with a neighboor
    if (sp_analysis_bey.rpl_from_mis_cnt)
        fprintf('\n   REPLACEMENT DIE MISSING: index_fail              BEYOND \n');
        for cur_rpl_ndx= 1:1:sp_analysis_bey.rpl_from_mis_cnt
            if (isempty(sp_analysis_bey.rpl_from_mis_serial{1}))
                cur_serial_mis = '';
            else
                cur_serial_mis = char(sp_analysis_bey.rpl_from_mis_serial(cur_rpl_ndx));
            end
            fprintf('\n\t\t\t\t\t%-4d\t\t%-s'...
                , sp_analysis_bey.rpl_from_mis_ndx(cur_rpl_ndx), cur_serial_mis);
        end
        fprintf('\n');
    end
    fprintf('\n');
end  % Reporting Beyond

% USER WANTS TO PERFORM THE SP:
if ( (sp_analysis.sp_complete_flag) ||  ( ( ( (~(sp_analysis.sp_complete_flag)) ) && input_params.sp_ok_do_partially_flag ) &&  (~(input_params.check_availab_only_flag))) )
    % DOING IT FOR REAL. USER WANTS TO DOIT AND EITHER IT IS COMPLETE, OR IT IS INCOMPLETE BUT USER WANTS TO DO PARTIAL: Perform SP
    % Comment out next line for debuging: jas_temp  serials  needs to look like: 'P162142.06.0002'
    if (sp_analysis.sp_use_good_cnt)
        if (~(input_params.check_availab_only_flag))
            fprintf('\n PERFORMING     SAMPLING PLAN FOR THE FOLLOWING %-4d part_bins  (FOR REAL) \n',sp_analysis.sp_use_good_cnt);
            str_msg = ' Updating   part_bin:';
            
        else
            fprintf('\n SIMULATING     SAMPLING PLAN FOR THE FOLLOWING %-4d part_bins   (NOTHING DONE) \n',sp_analysis.sp_use_good_cnt);
            str_msg = ' Simulating part_bin:';
        end
        for cur_ndx =1:1:sp_analysis.sp_use_good_cnt
            fprintf('\n %-s %-4d \t\tSerial: %-s \tFrom: %s \tTo: %-s'...
                ,str_msg,sp_analysis.sp_use_good_ndx(cur_ndx),char(sp_analysis.sp_use_good_serial{cur_ndx}),bin_number_from,bin_number_to);
            if (~(input_params.check_availab_only_flag))
                %error_value = 0; % jas_temp to debug
                [error_value, error_message] = updateUnitInventoryPartBin(obj, 'jsanchez',                                     char(sp_analysis.sp_use_good_serial{cur_ndx}),bin_number_to);
                if error_value ~= 0
                    fprintf('\n');
                    disp([' ERROR_WHILE PERFOMING SAMPLING PLAN: RunCard exception encountered (updateUnitInventoryPartBin):\n ' error_message]);
                    if (cur_ndx >1 )
                        error_message =  strcat( error_message, '\nError_value = ',num2str(error_value),'\nCount of Part Bin actually changed = ',num2str(cur_ndx-1));
                    else
                        error_message =  strcat( error_message, '\nError_value = ',num2str(error_value),'\nNo changes were made to RunCard');
                    end
                    return;
                end
            end
        end% for each die to be moved to a new bin
    else
        fprintf('\nPERFORMING SAMPLING PLAN FOR NO die\n');
    end
end
if (sp_analysis.sp_complete_flag)
    fprintf('\n\n\tSAMPLING PLAN: COMPLETE');
else
    fprintf('\n\n\tSAMPLING PLAN: IN-COMPLETE');
end
if (sp_analysis.rpl_from_ok_cnt + sp_analysis_bey.rpl_from_ok_cnt)
    fprintf('\n\tSAMPLING PLAN: USED REPLACEMENTS: %-5d ( BELOW: %-5d +  BEYOND: %-5d)\n'...
        ,sp_analysis.rpl_from_ok_cnt + sp_analysis_bey.rpl_from_ok_cnt ...
        ,sp_analysis.rpl_from_ok_cnt ...
        ,sp_analysis_bey.rpl_from_ok_cnt);
else
    fprintf('\n\tSAMPLING PLAN: did not use REPLACEMENTS');
end
fprintf('\n');

end % fn sp_part_bin_update

% Left overs:
% ... do some algorithmic logic to determine which bins to assign to which
% chip based on gel pack location
% ...
% Now update the chip part bin assignment per sampling bin number
% bin_number = sampling_plan_number + 1;            % jas_temp
% chip_serial_number          = 'P153060.06.0378';  % jas_temp


%   resp_avi_spot =  229�1 struct array with fields:
%     serial          'P153060.06.001.023'
%     partnum         'EXP_CHIP'
%     partrev         '01'
%     part_class      'CHIP-SPOTTED'
%     lotnum          'P153060'
%     qty             1
%     operator        'aosorio'
%     rework          0
%     workorder       'DWO-0021'
%     seqnum          40
%     opcode          'S380'
%     status          'COMPLETE'
%     factory         'SV01'
%     warehouseloc    'Consumable Inventory'
%     carrier_type    'GP12x10'
%     carrier_id      'LN180010GELPACK2'
%     carrier_row     5
%     carrier_col     2
%     warehousebin    'INVENTORY'
%     moddate         '2018-01-31 17:24:04'

% ryan's cmds by hand: [ resp_avi_spot, error, msg] = fetchInventoryItems(obj, '','', '', '', wo_number, op_code, seq_num, '', '');
% ryan's cmds by hand: [error, msg] = updateUnitInventoryPartBin(obj, 'jsanchez', 'P153060.06.0378','2');

% runcard inventory master:
% Serial:P153060.06.0378 Serial:P153060.06.0378 Serial:P153060.06.0378 Serial:P153060.06.0378 Serial:P153060.06.0378

