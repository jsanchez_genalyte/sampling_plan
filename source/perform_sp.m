function [ sp_analysis,sp_analysis_bey,error_message] = perform_sp( ...
    resp_avi_spot_table,resp_fvi_spot_table,die_index_avail,die_index_fail,sp_die_ndx,input_params )
%PERFORM_SP performs the requested sp given 2 tables: the available after vi and the failures from vi (visual inspection)
%   This is the algorithm to handle:
%   replace with neighboors, visual inspection failures, incomplete sampling plans etc.
% pipeline:  sampling_plan (main) -->  sp_part_bin_update --> perform_sp



error_message                   = '';
sp_complete_flag                = false;
sp_analysis                     = struct;
sp_analysis.sp_complete_flag    = false;
sp_analysis.analysis_done       = false;
first_beyond_flag               = false; % assume nothing beyond until something is found beyond.
sp_analysis.sp_use_good_cnt     = 0;
sp_analysis.resp_all_spot_cnt   = 0;
sp_analysis.resp_avi_spot_cnt   = 0;
sp_analysis.resp_fvi_spot_cnt   = 0;
sp_analysis.die_index_sp_ndx_cnt= 0;

sp_analysis.sp_filled_die_cnt   = 0;
sp_analysis.sp_mis_die_cnt      = 0;
sp_analysis.resp_fail_cnt       = 0;
sp_analysis.rpl_from_ok_cnt     = 0;  
sp_analysis.rpl_from_mis_cnt    = 0;

sp_analysis.rpl_from_mis_cnt    = 0;
sp_analysis.rpl_from_mis_cnt    = 0;


first_beyond_index              = 0;
sp_analysis_bey                 = struct; 
sp_analysis_bey.rpl_from_ok_cnt = 0;
sp_analysis_bey.rpl_from_mis_cnt= 0;

if (isempty(sp_die_ndx))
    error_message = 'ERROR: Empty Sampling Plan. Nothing Done';
    return;
end

%good_cnt                = resp_avi_spot_cnt;
resp_avi_spot_cnt        = height(resp_avi_spot_table);
resp_fvi_spot_cnt    	 = height(resp_fvi_spot_table);
die_index_sp_ndx_cnt     = size(sp_die_ndx,1);
die_index_sp_ndx_max     = max(sp_die_ndx);               %  && (cur_die_ndx <= die_index_sp_ndx_max)
%die_index_sp_ndx_ori    = sp_die_ndx;

if(resp_avi_spot_cnt ==0)
    error_message = 'ERROR: Work Order Has No die Available to perform SP. Nothing Done';
    return;
end


% Build sp_die_table (some serials will not get set:
serial                   = cell(die_index_sp_ndx_cnt,1); % start with empty serials for all sp
sp_mis_flag              = ones(die_index_sp_ndx_cnt,1); % This is to flag all the sp below and beyond what is on the table (assume missing until found)
rpl_from_flag            = zeros(die_index_sp_ndx_cnt,1);
rpl_with_ndx             = zeros(die_index_sp_ndx_cnt,1);
sp_die_table             = table(serial,sp_die_ndx,sp_mis_flag, rpl_from_flag,rpl_with_ndx);

% extend both tables with flags and die index .. then concatenate them
% FAILURES FROM SPOTTING VISUAL INSPECTION: fvi                             FVI
good_flag               = zeros( resp_fvi_spot_cnt,1);
sp_req_flag             = zeros(resp_fvi_spot_cnt,1);
sp_use_flag             = zeros(resp_fvi_spot_cnt,1);  % used: ie  fullfill in first attemt
sp_mis_flag             = zeros(resp_fvi_spot_cnt,1);  % missed to fullfill in first attempt
die_index               = die_index_fail;
if (isnan(die_index))
    die_index = zeros(0,1);
end
if (isempty(resp_fvi_spot_table))
    resp_fvi_table = table();
else
    resp_fvi_table          = [table(sp_req_flag) table(sp_use_flag)  table(sp_mis_flag) table(good_flag) table(die_index) resp_fvi_spot_table(:,1:20) ];
end

% GOOD AFER SPOTTING VISUAL INSPECTION: avi                                  AVI
good_flag               = ones(resp_avi_spot_cnt,1);
sp_req_flag             = zeros(resp_avi_spot_cnt,1);
sp_use_flag             = zeros(resp_avi_spot_cnt,1);
sp_mis_flag             = zeros(resp_avi_spot_cnt,1);
die_index               = die_index_avail;
if (isnan(die_index))
    die_index = zeros(0,1);
end
%die_index_avai_max      = max(die_index_avail);
resp_avi_table          = [table(sp_req_flag) table(sp_use_flag)  table(sp_mis_flag) table(good_flag) table(die_index) resp_avi_spot_table(:,1:20) ];


resp_all_chip_table     = [ resp_avi_table ; resp_fvi_table];

% create resp_all_chip_table as the concatenation of available and good_flag


% sort the table by die-index
resp_sort_chip_table    = sortrows(resp_all_chip_table,'die_index');

bin_not_from_ndx = resp_sort_chip_table.part_bin ~= input_params.bin_number_from;
if (sum(bin_not_from_ndx))
    % THE COMPLEMENT OF THE BIN_FROM IS NOT EMPTY: Remove them from the table by marking it as no-good
    fail_true_ndx                                     = find(resp_sort_chip_table.good_flag==0); % save it for rpt.
    other_sp_ndx                                      = find(resp_sort_chip_table.part_bin ~= input_params.bin_number_from & resp_sort_chip_table.part_bin ~= num2str(1));
    other_sp_bin                                      = resp_sort_chip_table.part_bin(other_sp_ndx); 
    other_sp_serial                                   = resp_sort_chip_table.serial(other_sp_ndx); 
    resp_sort_chip_table.good_flag(bin_not_from_ndx ) = 0;
else
    fail_true_ndx = nan;
    other_sp_ndx  = nan;
    other_sp_bin  = nan;
    other_sp_serial = nan;
end

% % % % Not a good idea: Try better: mark as failures the ones with not: input_params.bin_number_from
% % % bin_from_ndx = resp_sort_chip_table_all.part_bin == input_params.bin_number_from;
% % % % Build a smaller table with just the records availabe for the sampling plan
% % % resp_sort_chip_table    = resp_sort_chip_table_all(bin_from_ndx,:); 


resp_all_chip_cnt       = height(resp_all_chip_table);
rpl_from_flag           = zeros(resp_all_chip_cnt,1);    % replacement: non-zero: FROM == fail+requested
rpl_with_ndx            = zeros(resp_all_chip_cnt,1);    % replacement: non-zero:  WHTH ie the ndx used to replace
sp_avn_flag             = zeros(resp_all_chip_cnt,1);
resp_sort_chip_table    = [resp_sort_chip_table table(rpl_from_flag) table(rpl_with_ndx) table(sp_avn_flag)];


% HAVE SORTED BY DIE_INDEX CHIP TABLE WITH ALL: AVAILABLE AND FAILURE AND WITH 4 flags:
%  sp_req_flag (req by the selected sp)
% ,sp_use_flag (to be filled)
% ,mis_flag    (to be filled)
% ,good_flag   (no failures)


% Do first pass of sampling plan: _____________________________________________________________________________ FIRST PASSS _____
% Turn on the ndx with the requested sp;

fit_die_index_sp_ndx = find(sp_die_ndx <= resp_all_chip_cnt);
bey_die_index_sp_ndx = find(sp_die_ndx >  resp_all_chip_cnt);
cros_ndx             = zeros(resp_all_chip_cnt,1);
if (isempty(fit_die_index_sp_ndx))
    % ALL THE REQUESTED DIE FOR THE SAMPLING PLAN are BEYOND THE AVAILABLE CHIP indices:
    % The only way to fix this is (if replacement was requested)
    % 1 - fill as much as possible the request with the available chips.
    % error_message = 'ERROR: jas_tbd: handle logic when all the sp die ndx  are above the available indices';
    sp_die_ndx_last = 0;
    sp_die_table.sp_mis_flag(bey_die_index_sp_ndx) = 1;  % to be solved in pass 2 or 3
    %return;
    %sp_die_table.serial = {}; % (1:sp_die_ndx_last) = resp_all_chip_table.serial(fit_die_index_sp_ndx);
    % do the work with the below indices first:
    if ( length(fit_die_index_sp_ndx) == length(sp_die_ndx))
        % ALL OF THE SP DIE REQUEST ARE WITHIN WHAT IS THERE (avail+fail):
        resp_sort_chip_table.sp_req_flag(sp_die_ndx) = 1;  % flag the ones we need: I.E. All of them
    else
        % SOME OF THE SP DIE REQUEST ARE ABOVE WHAT IS AVAILABLE: (avail+fail) Mark as request only what can be done directly
        if (isempty(bey_die_index_sp_ndx))
            % ALL THE REQUESTED DIE FOR THE SAMPLING PLAN are BELOW THE AVAILABLE CHIP indices:    NORMAL CASE
            fprintf('\n logic error: we should not get here. Contact Jorge. 858 774 1273\n');
        else
            % SOME OF THE SP DIE REQUEST ARE ABOVE WHAT IS AVAILABLE: (avail+fail) Mark as request only what can be done directly
            % Mark the beyond only in the sp table
            sp_die_table.sp_mis_flag(bey_die_index_sp_ndx) = 1;  % to be solved in pass 2 or 3
        end
    end
    % HAVE THE sp_req_flag set for the resp_sort_chip_table. Set the cross_ndx
    
    % Build a cross index to locate in the sp any entry in the all_table
    cur_sp_ndx =1;
    for cur_all_ndx =1:1:resp_all_chip_cnt
        if (resp_sort_chip_table.sp_req_flag(cur_all_ndx))
            cros_ndx(cur_all_ndx) =  cur_sp_ndx;
            cur_sp_ndx             = cur_sp_ndx+1;
        end
    end
else
    % SOME OF THE REQUEST INDICES ARE BELOW THE END OF THE TABLE:
    % update serial for the sp_die_table
    sp_die_ndx_last =length(fit_die_index_sp_ndx);
    sp_die_table.serial(1:sp_die_ndx_last) = resp_all_chip_table.serial(sp_die_ndx(fit_die_index_sp_ndx));  % jas_fix_2018_04_10 vs:    resp_all_chip_table.serial(fit_die_index_sp_ndx);
    
    % do the work with the below indices first:
    if ( length(fit_die_index_sp_ndx) == length(sp_die_ndx))
        % ALL OF THE SP DIE REQUEST ARE WITHIN WHAT IS THERE (avail+fail):
        resp_sort_chip_table.sp_req_flag(sp_die_ndx) = 1;  % flag the ones we need: I.E. All of them
    else
        % SOME OF THE SP DIE REQUEST ARE ABOVE WHAT IS AVAILABLE: (avail+fail) Mark as request only what can be done directly
        % jas_tbd: Maybe the next line is not needed because it is not used jas_tbd_verify
        resp_sort_chip_table.sp_req_flag(sp_die_ndx(fit_die_index_sp_ndx)) = 1;  % flag subset: some of the ones we need.
        if (isempty(bey_die_index_sp_ndx))
            % ALL THE REQUESTED DIE FOR THE SAMPLING PLAN are BELOW THE AVAILABLE CHIP indices:    NORMAL CASE
            fprintf('\n logic error: we should not get here. Contact Jorge. 858 774 1273\n');
        else
            % SOME OF THE SP DIE REQUEST ARE ABOVE WHAT IS AVAILABLE: (avail+fail) Mark as request only what can be done directly
            % Mark the beyond only in the sp table
            sp_die_table.sp_mis_flag(bey_die_index_sp_ndx) = 1;  % to be solved in pass 2 or 3
        end
    end
    % HAVE THE sp_req_flag set for the resp_sort_chip_table. Set the cross_ndx
    
    % Build a cross index to locate in the sp any entry in the all_table
    cur_sp_ndx =1;
    for cur_all_ndx =1:1:resp_all_chip_cnt
        if (resp_sort_chip_table.sp_req_flag(cur_all_ndx))
            cros_ndx(cur_all_ndx) =  cur_sp_ndx;
            cur_sp_ndx             = cur_sp_ndx+1;
        end
    end
    
end % non empty fit die index

% FIRST PASS: COMPLETE. Do second pass ...
% Do second pass of sampling plan: _____________________________________________________________________________ SECOND PASSS _____

if ((resp_avi_spot_cnt >= die_index_sp_ndx_cnt) && (resp_all_chip_cnt >= die_index_sp_ndx_max))
    % SP HAS NOT TOO MANY DAY AND REQUESTED INDICES ARE NOT TOO LARGE:
    % we may be able to do a complete sp (144/204) deppending on failures 
    % Check if within the requested there are failures: sp_use_flag == requested and good good_flag
    req_and_good_ndx                                    = resp_sort_chip_table.sp_req_flag(sp_die_ndx) & resp_sort_chip_table.good_flag(sp_die_ndx);
    resp_sort_chip_table.sp_use_flag(sp_die_ndx(req_and_good_ndx))  = 1;
    resp_sort_chip_table.sp_mis_flag(sp_die_ndx(req_and_good_ndx))  = 0;
    sp_die_table.sp_mis_flag(req_and_good_ndx)                      = 0; %jas_fix_rev_107  2018_03_28
    
    use_pass_one_cnt                                    = sum(resp_sort_chip_table.sp_use_flag);
    if (use_pass_one_cnt == die_index_sp_ndx_cnt)
        % GOT IT ALL USE IS GOOD AND WE COVERED THE WHOLE SP: No miss and no need for replacements.
        sp_complete_flag                = true;
        sp_analysis.analysis_done       = true;
    else
        % DID NOT GET ALL WHAT IT WAS REQUESTED: SOME of the req IS not GOOD AND WE did not COVER THE WHOLE SP
        if (~(input_params.replace_chip_if_miss_flag ) )
            % USER DOES NOT WANT TO REPLACE MISSING
            % Determine which ones require replacement with neighboors:
            sp_complete_flag            = false;
            sp_analysis.analysis_done   = true;
        else
            % USER REQUESTED TO REPLACE MISSING: requested and not used
            resp_sort_chip_table.sp_mis_flag(sp_die_ndx)        = resp_sort_chip_table.sp_req_flag(sp_die_ndx) & (~(resp_sort_chip_table.sp_use_flag(sp_die_ndx)));
            sp_mis_cnt                                          = sum(resp_sort_chip_table.sp_mis_flag);
            resp_sort_chip_table.rpl_from_flag                  = resp_sort_chip_table.sp_mis_flag; %  start the replace from: with all the missing. to being with
            % Determine available neighboors: not used yet but good (all of these must be among the not requested: othewhise they would had been used)
            resp_sort_chip_table.sp_avn_flag                    = (~(resp_sort_chip_table.sp_use_flag)) & (resp_sort_chip_table.good_flag);
            sp_avn_cnt                                          = sum(resp_sort_chip_table.sp_avn_flag);
            if (sp_avn_cnt < sp_mis_cnt)
                % DID NOT GET IT: Not enough neighboors: Use them all: sp_avn_cnt     AVN == Available Neighbor.
                sp_complete_flag                                                    = false;
                resp_sort_chip_table.sp_use_flag( resp_sort_chip_table.sp_avn_flag) = 1; % does this stat. modifies only the ones?
                resp_sort_chip_table.sp_mis_flag(resp_sort_chip_table.sp_avn_flag)  = 0;
                sp_die_table.sp_mis_flag(cros_ndx(resp_sort_chip_table.sp_avn_flag))= 0;
                %only true replacements: resp_sort_chip_table.rpl_with_ndx(resp_sort_chip_table.sp_mis_flag) ~=0  fill in the "with"  with the available neighboors.
            else
                % GOT IT: ENOUGH neighboors: Use a subset. only sp_mis_cnt of them and the closest ones.
                sp_complete_flag        = true;
                %ava_size               = size(ava_ndx,1);
                %ava_back_flag          = zeros(ava_size,1);
                for cur_mis_ndx =1:1:sp_mis_cnt
                    % find nearest neighboor for this missing among the available ones.
                    % As you find them mark the sp_use_flag to 1 for the found nearerst neightboor.
                    [ resp_sort_chip_table,sp_die_table,found_repl_flag ] = find_closest_nei( resp_sort_chip_table,sp_die_table,cros_ndx );
                    if (~(found_repl_flag))
                        break;
                    end
                end % for each missing  tag: .sp_avn_flag     ava_ndx(ndx_1)
                % AT THIS POINT EACH MISSING SHOULD HAD BEEN REPLACED WITH THE NEAREST NEIGHBOOR: Evaluate
                if ( (sum(resp_sort_chip_table.sp_mis_flag) == 0) && (sum(resp_sort_chip_table.sp_use_flag) == die_index_sp_ndx_cnt))
                    %
                    fprintf('Succsess finding neightboors. SP WAS FULLY SATISFIED. See: sp_use_flag for used die.');
                end
            end % sp_avn_cnt > sp_mis_cnt (using only some of the neigh)
        end % USER req replace if missing.
    end % DID NOT GET IT all of the requested so some is not GOOD AND WE did not COVER THE WHOLE SP
else  % 48 /207
    % WE CAN **not** DO A COMPLETE SP. 3 possible cases but we do not care to know why. Just fill in as much as we can:
    if ( (~(input_params.sp_ok_do_partially_flag)) && (~(input_params.replace_chip_if_miss_flag)))
        % BAD SHAPE AND TOO RESTRICTIVE: can not do anyting to fix problem
        sp_complete_flag        = false;
    end
    if ( input_params.sp_ok_do_partially_flag)  
        % DOING_PARTIAL REQUEST AND MAY OR MAT NOT BE OK TO REPLACE IF MISSING
        %   Possible causes to prevent doing a full SP:
        %   1 - cnt     of avail is less than cnt     of sp: if doing_partial request:  trim sp to cnt_of avail
        %   2 - max ndx of avail is less than max ndx of sp: if doing_partial request:  trim sp to max_die_ndx <= max ndx of avail
        %   3 - 1 and 2: trim sp to the smallest of 1 and 2.
        % FIRST PASS INCOMPLETE:                                             ***************************    first pass incomplete **********************
        for cur_die_ndx = 1:1: die_index_sp_ndx_cnt
            % PROCESS EACH REQUEST ONE AT A TIME: first pass
            
            cur_die_req_ndx = sp_die_ndx(cur_die_ndx);   
            % check if good:
            if (cur_die_req_ndx <= resp_all_chip_cnt)
                if  (resp_sort_chip_table.good_flag(cur_die_req_ndx))
                    % WITHIN BOUNDS AND GOOD: Use it
                    resp_sort_chip_table.sp_use_flag(cur_die_req_ndx)       = 1;
                    resp_sort_chip_table.sp_mis_flag(cur_die_req_ndx)       = 0;
                    sp_die_table.sp_mis_flag(cros_ndx(cur_die_req_ndx))     = 0;
                    % no need for replace_from and with because it is available: there is no replacement here.
                else
                    % FAIL: Mark it as missing and skip it for the first pass:
                    resp_sort_chip_table.sp_mis_flag(cur_die_req_ndx)       = 1;
                    % mark it as needed to be replaced from: (without a replace with)
                    if (input_params.replace_chip_if_miss_flag)
                        % OK TO DO REPLACEMENT FOR MISSING: 
                        resp_sort_chip_table.rpl_from_flag(cur_die_req_ndx)     = 1;
                        sp_die_table.sp_mis_flag(cros_ndx(cur_die_req_ndx))     = 1;
                        sp_die_table.rpl_from_flag(cros_ndx(cur_die_req_ndx))   = 1;
                    end
                end
            else
                % JAS_HERE HANDLE REQUESTS BEYOND ... tues_eve
                % build list of die for the third pass: pass beyond what is on the  table
                % find the position in the sp for this index;
                % cur_sp_ndx = find(sp_die_table.sp_die_ndx ==cur_die_req_ndx,1);

                if (~(first_beyond_flag))
                    first_beyond_flag = true;
                    first_beyond_index = cur_die_req_ndx;
                end
                sp_die_table.sp_mis_flag(cur_die_ndx) = 1;
                if (input_params.replace_chip_if_miss_flag)
                    % OK TO DO REPLACEMENT FOR MISSING: check for the last available from the all table
                    resp_sort_chip_table.sp_avn_flag = (~(resp_sort_chip_table.sp_use_flag)) & (resp_sort_chip_table.good_flag);
                    ava_ndx                          = find(resp_sort_chip_table.sp_avn_flag == 1); %  = (~(resp_sort_chip_table.sp_use_flag)) & (resp_sort_chip_table.good_flag);
                    if (~(isempty(ava_ndx)))
                        % HAVE AVAILABLE: Take the last one (because we are beyond.
                        ava_ndx = ava_ndx(end);
                        sp_die_table.rpl_from_flag(cur_die_ndx)                 = 1;
                        sp_die_table.rpl_with_ndx(cur_die_ndx)                  = ava_ndx;
                        sp_die_table.sp_mis_flag(cur_die_ndx)                   = 0;           % as replacement was found: No failure anymore
                        resp_sort_chip_table.sp_mis_flag(ava_ndx)               = 0;           % as replacement was found: No failure anymore
                        resp_sort_chip_table.sp_use_flag(ava_ndx)               = 1;           % mark it as used, so it does no keep using the same one
                        % get the serial of the from
                        sp_die_table.serial(cur_die_ndx)                        = resp_sort_chip_table.serial(ava_ndx);
                    else
                        break;  % nothing found to work with... we must be done.
                    end
                end
             end
        end % for each die requested in the sp.
        
        if (first_beyond_flag)
            fprintf('\n Found Sampling Plan (SP) indices: \t\t\t  From  %-4d To %-4d are BEYOND the Available WO largest Index: %-4d\n'...
                ,first_beyond_index, sp_die_ndx(die_index_sp_ndx_cnt),   resp_all_chip_cnt);
        end
        
        % AT THIS POINT WE HAVE: What is used and what is missed due to failures or beyond: First pass.
        % DO A SECOND PASS: Fill the wholes as much as you can.
        % && (cur_die_ndx <= die_index_sp_ndx_max)
        %fprintf('done with first pass\n');
        for cur_die_ndx = 1:1: die_index_sp_ndx_cnt
            % PROCESS EACH REQUEST ONE AT A TIME: second pass  ***************************    second pass incomplete **********************
            
            if ((~(sp_die_table.sp_mis_flag(cur_die_ndx))) || (~(input_params.replace_chip_if_miss_flag)  )  )
                % THIS DIE IS NOT MISSING.  OR ( IT IS MISSING BUT IS NOT OK TO DO REPLACEMENTS: skip
                continue; % This one is not missing (set at first pass) go for the next one.
            end
            cur_die_req_ndx = sp_die_ndx(cur_die_ndx);
            if ( (cur_die_req_ndx > resp_all_chip_cnt ) || ( cur_die_ndx > resp_all_chip_cnt ))
                % REACHED END OF TABLE: SP is asking for more thant what is there.
                % check if there is ANYTHING NOT USED YET:
                avai_ndx = find(resp_sort_chip_table.good_flag ==1 & resp_sort_chip_table.sp_use_flag ==0);
                if (isempty(avai_ndx))
                    % COULD NOT FIND ANYTHING TO FULLFILL THIS MISSING: We are done: Get out.
                    break;
                end
                % FOUND EMPTY SLOT: replacement pass BEYOND all table: use sp_die_table to record it:
                % THE FROM IS BEYOND !!!! the end of the table. only the sp_use_flag is within.
                avai_ndx                                       = avai_ndx(1);
                sp_die_table.sp_mis_flag(cur_die_ndx)          = 0;            % it is not missing anymore
                resp_sort_chip_table.sp_use_flag(avai_ndx)     = 1;            % it is used now.
                sp_die_table.rpl_from_flag(cur_die_ndx)        = 1;            % it is replaced now
                sp_die_table.c(cur_die_ndx)         = avai_ndx;     % it is the replaced with.
                continue; % this one is done
            end
            % THIS ONE IS MISSING: IT NEEDS TO BE REPLACED IF IT CAN BE DONE:  Find the closest neigh good that is not in use:
            [ resp_sort_chip_table,sp_die_table,found_repl_flag ] = find_closest_nei( resp_sort_chip_table,sp_die_table,cros_ndx );
            if (~(found_repl_flag))
                break;
            end
        end % for each die from sp: second pass.
        % for all cases: Use every good die. period.  no need for more logic
        % jas_here: do not use indiscriminately:  resp_sort_chip_table.sp_use_flag   = resp_sort_chip_table.good_flag; %best delivery effort.
    end % input_params.sp_ok_do_partially_flag && ok to do replacement
end   %  WE can NOT  DO A COMPLETE SP

% BEYOND LAST PASS:       _______________________________________________________________________________________________________  BEYOND

% if (~(isempty(bey_die_index_sp_ndx)))
%     % SOME SP DIE ARE BEYOND THE ALL TABLE: 
%     % pull from the end: ie the last available 
%     bey_die_index_sp_ndx = bey_die_index_sp_ndx(end:-1:1);
%     for cur_sp_die = 1:1:length(bey_die_index_sp_ndx)
%         
%         
%     end % for each request beyond all
% end

% save cnts for reporting _______________________________________________________________________________________________________

sp_analysis.resp_avi_spot_cnt   = resp_avi_spot_cnt;                    % Available     chip count  (good after visual inspection)
sp_analysis.resp_fvi_spot_cnt   = resp_fvi_spot_cnt;                    % Failures      chip count  (fail after visual inspection)
sp_analysis.resp_all_spot_cnt   = resp_avi_spot_cnt +resp_fvi_spot_cnt; % Total cnt: good + fail
sp_analysis.die_index_sp_ndx_cnt= die_index_sp_ndx_cnt;                 % SP Requested  chip count
sp_analysis.sp_die_ndx          = sp_die_ndx;

% rpt failures: ndx and serial
fail_ndx                        = find(resp_sort_chip_table.good_flag==0);

if (isnan(fail_true_ndx))
    sp_analysis.resp_fail_cnt   = 0;
else
    sp_analysis.resp_fail_cnt   = length(fail_true_ndx);
end
sp_analysis.resp_fail_ndx       = fail_true_ndx;
if (isnan(fail_true_ndx))
    sp_analysis.other_sp_cnt    = 0;
else
    sp_analysis.other_sp_cnt    = length(other_sp_ndx);
end
sp_analysis.other_sp_ndx        = other_sp_ndx; 
sp_analysis.other_sp_bin        = other_sp_bin;
sp_analysis.other_sp_serial     = other_sp_serial;
if (isempty(resp_fvi_spot_table))
    sp_analysis.resp_fail_serial= {};    
else
    sp_analysis.resp_fail_serial= resp_fvi_spot_table.serial;
end
% sp_analysis.resp_fail_ndx(1), char(sp_analysis.resp_fail_serial(1)));

% evaluate results:
sp_filled_die_cnt               = sum(resp_sort_chip_table.sp_use_flag);  % SP filled  chip count
sp_mis_die_cnt                  = sum(resp_sort_chip_table.sp_mis_flag);  % SP miss  chip count
sp_mis_die_ndx                  = find(resp_sort_chip_table.sp_mis_flag == 1);  % SP miss  chip indices
sp_filled_die_index             = resp_sort_chip_table.die_index(logical(resp_sort_chip_table.sp_use_flag));
sp_use_all_ndx                  = find(resp_sort_chip_table.sp_use_flag ==1);
% determine sp_use_good_ndx
% get the good ones first

sp_use_good_ndx_ndx             = (resp_sort_chip_table.sp_use_flag ==1) & (resp_sort_chip_table.good_flag ==1) ;
sp_use_good_ndx                 = find(sp_use_good_ndx_ndx == 1);
sp_analysis.sp_complete_flag    = sp_complete_flag;    % partial sp or camplete
sp_analysis.sp_filled_die_cnt   = sp_filled_die_cnt;   % filled
sp_analysis.sp_mis_die_cnt      = sp_mis_die_cnt;      % missing to be filled
sp_analysis.sp_mis_die_ndx      = sp_mis_die_ndx;      % missing to be filled
sp_analysis.sp_filled_die_index = sp_filled_die_index; % selected die for the sp.
sp_analysis.sp_use_all_ndx      = sp_use_all_ndx;      % ndx of used for sp from all (before spotting)

if (~(isempty(sp_use_good_ndx)))
    sp_analysis.sp_use_good_cnt     = length(sp_use_good_ndx);
    sp_analysis.sp_use_good_ndx     = sp_use_good_ndx;     % ndx of used for sp from the good ones
    sp_analysis.sp_use_good_serial  = resp_sort_chip_table.serial(sp_use_good_ndx);  % serials used for sp from the good ones
else
    % FOUND NOTHING TO MOVE:
    sp_analysis.sp_use_good_cnt     = 0;
    sp_analysis.sp_use_good_ndx     = nan;     % ndx of used for sp from the good ones
    sp_analysis.sp_use_good_serial  = {};  % serials used for sp from the good ones
    
end

%resp_sort_chip_table.rpl_from_ok_ndx:      REPACEMENT: SUCCESS WITHIN
sp_analysis.rpl_from_ok_ndx     = find(resp_sort_chip_table.rpl_from_flag ==1 & resp_sort_chip_table.rpl_with_ndx ~=0 );
sp_analysis.rpl_from_ok_serial  = resp_sort_chip_table.serial(      sp_analysis.rpl_from_ok_ndx);
sp_analysis.rpl_with_ok_ndx     = resp_sort_chip_table.rpl_with_ndx(sp_analysis.rpl_from_ok_ndx);
sp_analysis.rpl_with_ok_serial  = resp_sort_chip_table.serial(resp_sort_chip_table.rpl_with_ndx(sp_analysis.rpl_from_ok_ndx));
sp_analysis.rpl_from_ok_cnt     = length(sp_analysis.rpl_from_ok_ndx);

%resp_sort_chip_table.rpl_from_miss_ndx:    REPACEMENT: MISS. WITHIN
sp_analysis.rpl_from_mis_ndx  	= find(resp_sort_chip_table.rpl_from_flag ==1 & resp_sort_chip_table.rpl_with_ndx ==0 );
sp_analysis.rpl_from_mis_cnt   	= length(sp_analysis.rpl_from_mis_ndx);
sp_analysis.rpl_from_mis_serial	= resp_sort_chip_table.serial(sp_analysis.rpl_from_mis_ndx);

% BEYOND: sp_analysis_bey:
% fix: 2018_04_10: Alex wants the die to come in original order: The beyond come backwards: 
%                  dis is due that for the first beyond we look for the last available, the second beyond gets the previous to last availabe and so on...
% i.e.
%   REPLACEMENT DIE OK:           AVAILABLE
%         index_from      index_with      serial_from             serial_replace      BEYOND
%         245                     242                     P153060.07.1096         P153060.07.1096
%         247                     241                     P153060.07.1095         P153060.07.1095
%         249                     240                     P153060.07.1094         P153060.07.1094
% but Alex wants: (maintain order)
%    REPLACEMENT DIE OK:           AVAILABLE
%         index_from      index_with      serial_from             serial_replace      BEYOND
%         245                     240                     P153060.07.1094         P153060.07.1094
%         247                     241                     P153060.07.1095         P153060.07.1095
%         249                     242                     P153060.07.1096         P153060.07.1096
% Solution: sort the rpl_with_ndx for the  beyond part of the sp_die_table in ascending order. 
% sp_die_table = 
%          serial          sp_die_ndx    sp_mis_flag    rpl_from_flag    rpl_with_ndx
%     _________________    __________    ___________    _____________    ____________
%     'P153060.07.0865'     11           0              1                  0         
%     'P153060.07.0866'     49           0              0                  0         
%     'P153060.07.0867'     62           0              0                  0         
%     'P153060.07.0868'    111           0              0                  0         
%     'P153060.07.0869'    132           0              0                  0         
%     'P153060.07.1096'    245           0              1                242    becomes: 240     
%     'P153060.07.1095'    247           0              1                241         
%     'P153060.07.1094'    249           0              1                240    becomes: 242

if (~(isempty(bey_die_index_sp_ndx)))
    sp_die_table_bey       = sp_die_table(bey_die_index_sp_ndx,:);
    rpl_with_ndx_descend   = sp_die_table_bey.rpl_with_ndx;  
    rpl_with_ndx_ascend    = sort(rpl_with_ndx_descend);
    sp_die_table.rpl_with_ndx(bey_die_index_sp_ndx)  = rpl_with_ndx_ascend;
end

if (~(isempty(bey_die_index_sp_ndx)))
    % SOMETHING TO RPT: there are sp die beyond ALL table
    
    %sp_die_table_bey = sp_die_table(1:length(fit_die_index_sp_ndx);
    % REPLACEMENT SUCCESS: BEYOND TABLE LIMITS:
    sp_analysis_bey.rpl_from_ok_ndx         = find(sp_die_table.rpl_from_flag ==1 & sp_die_table.rpl_with_ndx ~=0 );
    if (~(isempty(sp_analysis_bey.rpl_from_ok_ndx )))
        keep_ndx = sp_analysis_bey.rpl_from_ok_ndx > sp_die_ndx_last;
        sp_analysis_bey.rpl_from_ok_ndx         = sp_analysis_bey.rpl_from_ok_ndx(keep_ndx); % keep only the true beyond
        if (~(isempty(sp_analysis_bey.rpl_from_ok_ndx)))
            sp_analysis_bey.rpl_from_ok_serial  = sp_die_table.serial(      sp_analysis_bey.rpl_from_ok_ndx);
            sp_analysis_bey.rpl_with_ok_ndx     = sp_die_table.rpl_with_ndx(sp_analysis_bey.rpl_from_ok_ndx);
            sp_analysis_bey.rpl_with_ok_serial  = sp_die_table.serial(      sp_analysis_bey.rpl_from_ok_ndx);
            sp_analysis_bey.rpl_from_ok_cnt     = length(                   sp_analysis_bey.rpl_from_ok_ndx);
        end
    end
    
    % REPLACEMENT MISS: BEYOND TABLE LIMITS:
    sp_analysis_bey.rpl_from_mis_ndx        = find(sp_die_table.rpl_from_flag ==1 & sp_die_table.rpl_with_ndx ==0 );
    if (~(isempty(sp_analysis_bey.rpl_from_mis_ndx )))
        keep_ndx = sp_analysis_bey.rpl_from_mis_ndx > sp_die_ndx_last;
        sp_analysis_bey.rpl_from_mis_ndx         = sp_analysis_bey.rpl_from_mis_ndx(keep_ndx); % keep only the true beyond
        if (~(isempty(sp_analysis_bey.rpl_from_mis_ndx )))
            sp_analysis_bey.rpl_from_mis_ndx    = sp_analysis_bey.rpl_from_mis_ndx(sp_die_ndx_last+1:end);
            sp_analysis_bey.rpl_from_mis_cnt    = length(sp_analysis_bey.rpl_from_mis_ndx);
            sp_analysis_bey.rpl_from_mis_serial = sp_die_table.serial(sp_analysis_bey.rpl_from_mis_ndx);
        end
    end
end

% finall summary:
if (sp_analysis.sp_filled_die_cnt  == die_index_sp_ndx_cnt) 
    % sp_filled_die_cnt is total: from before and beyond!!! 
    sp_analysis.sp_complete_flag = true;
else
    sp_analysis.sp_complete_flag = false;
end

end % fn perform sp _____________________________________________________________________________________________________________________
% resp_all_chip_table.Properties.VariableNames'
%     'sp_req_flag'     % requested indices by the sp
%     'sp_use_flag'     % used by the sp (only from available)
%     'sp_mis_flag'     % misseed to be used in first pass,(in sec pass: some 1s may become zeros)
%     'good_flag'
%     'die_index'
%     'serial'
%     'partnum'
%     'partrev'
%     'part_class'
%     'lotnum'
%     'qty'
%     'operator'
%     'rework'
%     'workorder'
%     'seqnum'
%     'opcode'
%     'status'
%     'factory'
%     'warehouseloc'
%     'carrier_type'
%     'carrier_id'
%     'carrier_row'
%     'carrier_col'
%     'warehousebin'







