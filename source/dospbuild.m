% script: dobuild
% ref:    building sampling plan executable: hardcoded: run_card production or run_card_test   alex vs mario part_number
close all;
clear;
clear obj;
clear all;


cache_dir                  = 'C:\Users\jsanchez\Documents\Temp\jsanchez\mcrCache9.1\';

% Check if the cfg folder exists:
if ( ( exist(cache_dir,'dir') == 7))
    % Folder Exist and it is a directory:  If dir exists: Remove all cache dirs
    cd  (cache_dir);
    % CMD for loop to recursivelly remove all dirs: To start clean and avoid compiling with the wrong runcard db.
    !for /d %G in ("sampl*") do rd /s /q "%~G"
end

% WE ARE CLEAN: Ready to compile: 

cd  ('C:\Users\jsanchez\Documents\sampling_plan\source\');
utils_save_all_editor_open_files;

% to compile:
mcc     -m  sampling_plan -a ..\configure    -o  sampling_plan
%   mcc -m sampling_plan  -a ..\configure  -o  sampling_plan -R 'logfile','sampling_plan.log'
% -a @runcard_wsdl
!copy sampling_plan.exe C:\Users\jsanchez\Documents\sampling_plan\exe\
!move sampling_plan.exe \\genstore2\Users\Storage\jsanchez\Documents\sampling_plan\exe\
% !C:\Users\jsanchez\Documents\sampling_plan\exe\sampling_plan.exe

% ml_doc: https://www.mathworks.com/products/compiler/supported/compiler_support.html
% h = matlab.desktop.editor.getAll; h.close
% cd 'C:\Users\jsanchez\Documents\algo2\source'
% cd 'C:\Users\jsanchez\Documents\fcc\source'
% cd 'C:\Users\jsanchez\Documents\sampling_plan\source'

%   ~/Documents/Temp/jsanchez/mcrCache9.1/sampli1/sampling_pla/@runcard_wsdl

% Original character   Escaped character
% "                    &quot;
% '                    &apos;
% <                    &lt;
% >                    &gt;
% &                    &amp;



