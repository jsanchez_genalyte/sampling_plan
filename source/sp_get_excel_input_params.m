function [  input_params_struct, error_message  ] = ...
    sp_get_excel_input_params( an_excel_file_name,excel_tab_number)
%SP_GET_EXCEL_INPUT_PARAMS  returns a cell with the set input params:
% Input:  an_excel_file_name  Excel file name (full path) with data to be processed with the sampling_plan app.
%         excel_tab_number    Excel tab number(BE AWARE THAT EVEN HIDEN TABS ARE PART OF EXCEL NUMBERING:
%                             Warning: Do not hide tabs in excel: It is very misleading.
%
% Output: a cell with 110 entries: in two columns:
% Assumptions:
%   1 - column one is the parameter name
%   2 - column 2 is the parameter value
%   3 - column 3 is the description: Ignored.
%   4 - the indices are in ascending order. and there are no dupplicated indices.error_message  
error_message           = '';
input_params_struct     = struct;
input_params_cnt        = 13; % jas_jardcoded: number of expected input parameters in the excel file. Beyond that it will be ignored.
%input_params_names    	= cell(input_params_cnt,1);
%input_params_values   	= cell(input_params_cnt,1);           % up to excel_max_block_count analytes: names of data columns.

% read the whole sheet as a table:
sheet_table= readtable(an_excel_file_name...
    ,'ReadVariableNames',false,'FileType','spreadsheet'...
    ,'Sheet',excel_tab_number  );
cur_row             = 0;
row_found_cnt       = size(sheet_table,1); %
col_found_cnt       = size(sheet_table,2); %
if ( (col_found_cnt <2) ||  (row_found_cnt <input_params_cnt) )
    error_message = 'ERROR: Invalid Excel file format. Reading input parameters. Fix it and try again. \nExpect at least 2 cols and 11 rows!';
    return;
end

try
    input_params_names  = table2cell(sheet_table(2:input_params_cnt+1,1)); % first col is the  sp_names
    input_params_names  = strrep(input_params_names,'''','');
    input_params_values = table2cell(sheet_table(2:input_params_cnt+1,2)); % second col is the sp_values
    
    cfg_params_wanted_cel = { 'sp_master_excel_file_name','sampling_plan_number_to_use ','wo_number','seq_num'...
        ,'op_code','op_code_previous_step','bin_number_from','bin_number_to' ...
        ,'webservice_url' ,'part_number' ...
        ,'sp_ok_do_partially_flag','replace_chip_if_miss_flag','check_availab_only_flag'};
    
        cfg_params_wanted_struct = struct('sp_master_excel_file_name',''... 
            ,'sampling_plan_number_to_use',''... 
            ,'wo_number',''... 
            ,'seq_num',''...
            ,'op_code',''...
            ,'op_code_previous_step',''...
            ,'bin_number_from',''...
            ,'bin_number_to',''...
            ,'webservice_url',''...
            ,'part_number',''...            
            ,'replace_chip_if_miss_flag',''...
            ,'sp_ok_do_partially_flag',''...
            ,'check_availab_only_flag','');
    
    found_fields = isfield(cfg_params_wanted_struct, input_params_names);
    if (sum(found_fields) == input_params_cnt)
        % FOUND ALL THE NEEDED PARAMETERS: Assume they are in order.
        cur_row = 1;
        input_params_struct.sp_master_excel_file_name   = input_params_values{1};
        cur_row = 2;
        input_params_struct.sampling_plan_number_to_use = str2double(input_params_values{2});
        cur_row = 3;
        input_params_struct.wo_number                   = input_params_values{3};
        cur_row = 4;
        input_params_struct.seq_num                     = input_params_values{4};
        cur_row = 5;
        input_params_struct.op_code                     = input_params_values{5};
        cur_row = 6;
        input_params_struct.op_code_previous_step       = input_params_values{6};        
        cur_row = 7;
        input_params_struct.bin_number_from             = input_params_values{7};
        cur_row = 8;
        input_params_struct.bin_number_to               = input_params_values{8};
        
        cur_row = 9;
        input_params_struct.webservice_url              = input_params_values{9};
        cur_row = 10;
        input_params_struct.part_number                 = input_params_values{10};
        
        
        cur_row = 11;
        if (strcmpi(input_params_values{11},'TRUE'))
            input_params_struct.replace_chip_if_miss_flag   = 1;
        else
            input_params_struct.replace_chip_if_miss_flag   = 0;
        end
        cur_row = 12;
        if (strcmpi(input_params_values{12},'TRUE'))
            input_params_struct.sp_ok_do_partially_flag     = 1;
        else
            input_params_struct.sp_ok_do_partially_flag     = 0;
        end
        cur_row = 13;
        if (strcmpi(input_params_values{13},'TRUE'))
            input_params_struct.check_availab_only_flag   = 1;
        else
            input_params_struct.check_availab_only_flag   = 0;
        end
    else
        error_message = 'ERROR: Invalid Excel file format. Fix it and try again:\n';
        fprintf('\n%s',error_message);        
        fprintf('\n sp_master_excel_file_name   = %-s',input_params_values{1});
        fprintf('\n sampling_plan_number_to_use = %-s',input_params_values{2});
        fprintf('\n wo_number                   = %-s',input_params_values{3});
        fprintf('\n seq_num                     = %-s',input_params_values{4});
        fprintf('\n op_code                     = %-s',input_params_values{5});
        fprintf('\n op_code_previous_step       = %-s',input_params_values{6});        
        fprintf('\n bin_number_from             = %-s',input_params_values{7});
        fprintf('\n bin_number_to               = %-s',input_params_values{8});
        fprintf('\n webservice_url              = %-s',input_params_values{9});
        fprintf('\n part_number                 = %-s',input_params_values{10});        
        fprintf('\n sp_ok_do_partially_flag     = %-s',input_params_values{11});
        fprintf('\n replace_chip_if_miss_flag   = %-s',input_params_values{12});
        fprintf('\n check_availab_only_flag     = %-s',input_params_values{13});
        return;
    end
catch
    error_message = sprintf('ERROR: Invalid Input Parameters Excel file format. \nFix it and try again. Error Reading row: %4d',cur_row);
    return;
end % catch
fprintf('\n Configuration Parameters:    \n');
fprintf('\n sp_master_excel_file_name   = %-s',input_params_values{1});
fprintf('\n sampling_plan_number_to_use = %-s',input_params_values{2});
fprintf('\n wo_number                   = %-s',input_params_values{3});
fprintf('\n seq_num                     = %-s',input_params_values{4});
fprintf('\n op_code                     = %-s',input_params_values{5});
fprintf('\n op_code_previous_step       = %-s',input_params_values{6});
fprintf('\n bin_number_from             = %-s',input_params_values{7});
fprintf('\n bin_number_to               = %-s',input_params_values{8});
fprintf('\n webservice_url              = %-s',input_params_values{9});
fprintf('\n part_number                 = %-s',input_params_values{10});
fprintf('\n replace_chip_if_miss_flag   = %-s',input_params_values{11});
fprintf('\n sp_ok_do_partially_flag     = %-s',input_params_values{12});
fprintf('\n check_availab_only_flag     = %-s',input_params_values{13});
end % fn sp_get_excel_input_params

