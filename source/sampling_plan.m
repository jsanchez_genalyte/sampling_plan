% Main file: sampling_plan.m
% Script to perform the sampling plan given 2 excel files: the master file and the config.
% fcc_app:
% %       h = matlab.desktop.editor.getAll; h.close
%     cd 'C:\Users\jsanchez\Documents\fcc\source\'
% to compile:
%   mcc -m  sampling_plan -a ..\configure  -o  sampling_plan
%   mcc -m dcrc_gen_xml  -a ..\configure  -o  sampling_plan -R 'logfile','sampling_plan.log'

%   !copy sampling_plan.exe C:\Users\jsanchez\Documents\sampling_plan\exe\
%   !move sampling_plan.exe \\genstore2\Users\Storage\jsanchez\Documents\sampling_plan\exe\
%   ml_doc: https://www.mathworks.com/products/compiler/supported/compiler_support.html

% Define Arguments: (see end of this file)

% to test:  runcard_test: use wo: DWO-0051 

% pipeline: sampling_plan --> get_sp_input_params  --> sp_get_excel_input_params (excel) 
%                         --> sp_part_bin_update   --> sp_read_sp_master_file    -->  sp_get_excel_range (excel) 
%                                                  --> perform_sp                -->|
% Rev_history
% Rev 1.00 Date: 2018_03_21 First compiled version: Test only  
% Rev 1.01 Date: 2018_03_21 First verification version by Jas:  
% Rev 1.02 Date: 2018_03_23 First verification version by Mario: Cleaned output Test and Production.
% Rev 1.03 Date: 2018_03_26 Add dbg: Dump Tables: to sort out to which runCard I was connecting to.
% Rev 1.04 Date: 2018_03_27 test Removed all dbg to solve issue of runcard_test vs runcard
%                lesson: Remove dir: ~/Documents/Temp/jsanchez/mcrCache9.1/sampli*/*
%                ie cd ~/Documents/Temp/jsanchez/  rm -r mcrCache9.1
% Rev 1.05 Date: 2018_03_27 production 
% Rev 1.07 Date: 2018_03_28 production  fixed indexing: cross_ndx %jas_fix_rev_107  2018_03_28
% Rev 1.08 Date: 2018_04_10 production  fixed: beyond: from reverse order to wf order.
%                                       added: changes to support 2 sp in a row.
% Rev 1.09 Date: 2018_04_12 production  fixed part number for retrieving failures. 
% Rev 1.10 Date: 2018_04_19 production  fixed length(nan)=1 --> check for nan before taking lenght (fail_cnt=1 when there were no failures 
% Rev 1.11 Date: 2018_05_10 production  add (Robustness) check for inconsistent request: the sp cnt and the indices (non-blank cnts) in the master file. 
% Rev 1.12 Date: 2018_06_30 production  fix: categorical does not work on char variables: Error in sp_part_bin_update (line 98)
%                                            Can not create a categorical array from a character array.  Added: cellstr to convert to a cell array of character vectors.


run_date = datestr(now);
fprintf('\n Initializing Sampling Plan:   Rev 1.12  Production \n Run Date-time                 %-s\n',run_date);
[ input_params,error_message ] = get_sp_input_params();
if (~(isempty(error_message)))
    fprintf('\n ERROR:\n%s\n',error_message);
    fprintf('\n');
    pause(20);
    return;
end
[ error_message ]  = sp_part_bin_update( input_params);
if ((~(isempty(error_message))) && (~(strcmpi(error_message,'Inventory part bin updated'))))
    fprintf('\nERROR:,\n%s\n',error_message);
end
fprintf('\n\tANALYSIS OF SAMPLING PLAN REQUEST DONE_________________________\n\n'); 
% _______________________________________________________________________________________________________
if (isdeployed)
    fprintf('\n To produce a TEXT report of this output:  \n');    
    fprintf('\n    1. Copy to the clipboard all the output do: Edit_menu --> Select All --> Enter  ');
    fprintf('\n    2. Paste into an email message: CTRL-V to share the report with the Sampling Plan Requester. \n');      
    fprintf('\n To verify results: Follow these steps in RunCard:  \n');  
    fprintf('\n   1. Search for the needed WO in Terminal');  
    fprintf('\n   2. In the terminal view select the Step at which the carrier view is needed and click on the step number.');  
    fprintf('\n   3. In the View List drop-down select carrier view.');  
    fprintf('\n   4. The colors of each Gelpack in carrier view should reflect the new part_bin_numbers');
    fprintf('\n\n Bye ... \n');    
    % ALLOW THE USER TO DO CTRL-A CTRL-C
    pause(40); 
end

% sp_master_configure_file_name   = 'C:\Users\jsanchez\Documents\sampling_plan\configure\sampling_plan_configure.xlsm';   % 1
% sampling_plan_number_to_use     =  11; % 12         % 11 and 12 for   0051                                            % 2
% wo_number                       =  'DWO-0051'; % 'DWO-0058'; % 'DWO-0051'; % DWO-0052' ; % 'DWO-0058'; % 'DWO-0021';     % 3
% seq_num                         =  '40';                                                     % 4
% op_code                         =  'D320'; %'S380';                                          % 5
% bin_number_from                 =  '1';                                                      % 6
% bin_number_to                   =  '2';                                                      % 7
% 
% check_availab_only_flag     = true;  % DEFAULT = true.                                  % 8
% % true  == Just check if it is possible to do the sp. Do not change any bins.
% % false == Attempt to move the chips to the proper bin. See next flag.
% sp_ok_do_partially_flag     = true; % DEFAULT == false.                                 % 9
% %                                    % true  == Perform partial sampling plans and report which part was done and which was not.
% %                                    false == Move the chips to the proper bin: ONLY IFF the sp can be done completely
% replace_chip_if_miss_flag   = true;  % DEFAULT == false.                                % 10   
% %                                    % true  == attempt to find nearest neightboor for missing chips
% %                                    % false == Do not attempt to do replacements (see sp_ok_do_partially_flag)
% % flags: 1_ sp_ok_do_partially_flag 2_ replace_if_chip_is_miss_flag 3_ check_availab_only_flag


% 
% input_params.sampling_plan_number_to_use  = sampling_plan_number_to_use ;
% input_params.sp_master_excel_file_name    = sp_master_excel_file_name   ;
% input_params.wo_number                    = wo_number                   ;
% input_params.seq_num                      = seq_num                     ; 
% input_params.op_code                      = op_code                     ;
% input_params.bin_number_from              = bin_number_from             ;
% input_params.bin_number_to                = bin_number_to               ;
% input_params.check_availab_only_flag      = check_availab_only_flag     ;
% input_params.sp_ok_do_partially_flag      = sp_ok_do_partially_flag     ;
% input_params.replace_chip_if_miss_flag    = replace_chip_if_miss_flag   ;