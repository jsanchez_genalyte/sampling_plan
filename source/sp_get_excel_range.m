function [ sampling_plan, sp_names_cel, error_message  ] = ...
    sp_get_excel_range( an_excel_file_name,excel_tab_number)
%SP_GET_EXCEL_RANGE returns a cell with the set of ranges of data: one per block of data in the given sheet_table.
% Input:  an_excel_file_name  Excel file name (full path) with data to be processed with the std_curve_fit app.
%         excel_tab_number    Excel tab number(BE AWARE THAT EVEN HIDEN TABS ARE PART OF EXCEL NUMBERING:
%                             Warning: Do not hide tabs in excel: It is very misleading.
% Output: a cell with one or more entries: one entry per sp found. and each cell has a vector of die indices.
% Assumptions:
%   1 - There is one or more cells with this key: "sampling_plan_xxx"
%   2 - There is data for at least 1 sp in the file.
%   3 - the second row specifies the length of the sp
%   4 - the indices are in ascending order. and there are no dupplicated indices.
% pipeline:  sampling_plan  -->  sp_part_bin_update --> sp_read_sp_master_file --> sp_get_excel_range
error_message          = '';

sampling_plan_max_cnt   = 1000; % jas_jardcoded: 1000 sampling plans
sampling_plan           = {sampling_plan_max_cnt,1};  % (sampling_plan_max_cnt,1);
sp_names_cel            = cell(sampling_plan_max_cnt,1);           % up to excel_max_block_count analytes: names of data columns.
sp_wanted_keyword       = 'SAMPLING_PLAN';                        % **** EXCEL DEPPENDENCY **** Not it is an argument to this function

% read the whole sheet as a table:
sheet_table= readtable(an_excel_file_name...
    ,'ReadVariableNames',false,'FileType','spreadsheet'...
    ,'Sheet',excel_tab_number  );

sp_cnt       = size(sheet_table,2); %
if (sp_cnt <1)
    error_message = 'ERROR: Invalid Excel file format. Reading Sampling Plans. Fix it and try again';
    return;
end
sp_names_cel = table2cell(sheet_table(1,1:end)); % first row is the  sp_names
sp_len_cel   = table2cell(sheet_table(2,1:end)); % second row is the sp_lengths
try
    for cur_col=1:1:sp_cnt
        cur_sp_name_ndx     = strfind(upper(sp_names_cel{cur_col}),sp_wanted_keyword);
        if (~(isempty(cur_sp_name_ndx)))
            cur_sp_len_value        = str2double(sp_len_cel{cur_col});
            sampling_plan{cur_col}  = cell2matdouble(table2array(sheet_table(3:cur_sp_len_value+2,cur_col))); % 3rd to end is indices.
        end
        
    end
catch
    if (cur_col <= 1 )
        error_message = 'ERROR: Invalid Excel file format. Fix it and try again';
        return;
    else
        sp_cnt        = cur_col-1;
        sampling_plan = sampling_plan(1:sp_cnt);
        sp_names_cel  = sp_names_cel(1:sp_cnt);
        error_message = sprintf('WARNING: Invalid Excel file format. read only %-4d sampling plans',sp_cnt);
        return;
    end
end % catch

sampling_plan = sampling_plan(1:sp_cnt);
sp_names_cel  = sp_names_cel(1:sp_cnt);
end % fn sp_get_excel_range

