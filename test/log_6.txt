>> do 1 to 4 sp2  (74 parts)
>> do                                       1 to 4 sp2  (74 parts)

 Initializing Sampling Plan Rev 1.00   ...  

 DONE Reading Input Parameters file:

 sp_master_excel_file_name   = C:\Users\jsanchez\Documents\sampling_plan\configure\sampling_plan_master_official_nick_test_1.xlsm
 sampling_plan_number_to_use = 2
 wo_number                   = DWO-0051
 seq_num                     = 40
 op_code                     = D320
 op_code_previous_step       = A230
 bin_number_from             = 1
 bin_number_to               = 4
 check_availab_only_flag     = false
 sp_ok_do_partially_flag     = true
 replace_chip_if_miss_flag   = true
 DONE                      ... reading CONFIG PARAMETERS

 Reading ... Sampling plan Master File (excel):	   C:\Users\jsanchez\Documents\sampling_plan\configure\sampling_plan_master_official_nick_test_1.xlsm

 DONE  ... reading Sampling Plans: 	   2   sampling plans
Retrieving document at 'http://10.0.2.226/runcard_test/soap?wsdl'


 ok

 Found SP indices: From  0    To 726  Larger than the Available WO indices  242 
done with first pass

 WORK ORDER CHIP SUMMARY:
    Total into spotting      chip count       242  
    Available good           chip count       242  
    Failures (Visual Insp)   chip count       0    

 SAMPLING PLAN REQUEST FLAGS SUMMARY:
    Check Availability Only (just check) FLAG FALSE
    Sampling_plan_ok_do_partial          FLAG TRUE
    Replace chip if is missing           FLAG TRUE

 SAMPLING PLAN DETAILS
   Sampling plan name   to use:               Sampling_plan 2
   Sampling plan number to use:               2  
   Requested            SP chip count         74   

 SAMPLING PLAN SUMMARY RESULTS
 Sampling plan can be done completely:        TRUE 
   Filled              SP chip count          74   
   Missing             SP chip count          0    

 SAMPLING PLAN DETAIL RESULTS: 

   REQUESTED SAMPLING PLAN Die index  
		1   		2   		3   		4   		5   		6   		7   		8   		9   		10  
		11  		12  		13  		14  		15  		16  		17  		18  		19  		20  
		21  		22  		23  		24  		25  		26  		27  		28  		29  		30  
		31  		32  		33  		34  		35  		36  		359 		369 		379 		389 
		399 		409 		419 		429 		439 		449 		459 		468 		478 		488 
		498 		508 		518 		527 		538 		548 		558 		568 		578 		587 
		597 		607 		617 		627 		637 		647 		657 		667 		677 		686 
		697 		706 		716 		726 


   REPLACEMENT DIE: None

   REPLACEMENT DIE MISSING: None (All replacement needed where done fine)

   Filled    BEYOND    WORK ORDER chip count          38   
   Missing   BEYOND    WORK ORDER chip count          0    
   REPLACEMENT DIE OK:    BEYOND AVAILABLE
	index_from  	index_with   			serial_from  		serial_replace BEYOND AVAILABLE
	37  			242 			P162142.06.0895		P162142.06.0895
	38  			241 			P162142.06.0894		P162142.06.0894
	39  			240 			P162142.06.0352		P162142.06.0352
	40  			239 			P162142.06.0351		P162142.06.0351
	41  			238 			P162142.06.0350		P162142.06.0350
	42  			237 			P162142.06.0349		P162142.06.0349
	43  			236 			P162142.06.0348		P162142.06.0348
	44  			235 			P162142.06.0347		P162142.06.0347
	45  			234 			P162142.06.0346		P162142.06.0346
	46  			233 			P162142.06.0345		P162142.06.0345
	47  			232 			P162142.06.0343		P162142.06.0343
	48  			231 			P162142.06.0342		P162142.06.0342
	49  			230 			P162142.06.0341		P162142.06.0341
	50  			229 			P162142.06.0340		P162142.06.0340
	51  			228 			P162142.06.0339		P162142.06.0339
	52  			227 			P162142.06.0338		P162142.06.0338
	53  			226 			P162142.06.0337		P162142.06.0337
	54  			225 			P162142.06.0336		P162142.06.0336
	55  			224 			P162142.06.0335		P162142.06.0335
	56  			223 			P162142.06.0334		P162142.06.0334
	57  			222 			P162142.06.0333		P162142.06.0333
	58  			221 			P162142.06.0332		P162142.06.0332
	59  			220 			P162142.06.0331		P162142.06.0331
	60  			219 			P162142.06.0330		P162142.06.0330
	61  			218 			P162142.06.0329		P162142.06.0329
	62  			217 			P162142.06.0328		P162142.06.0328
	63  			216 			P162142.06.0327		P162142.06.0327
	64  			215 			P162142.06.0326		P162142.06.0326
	65  			214 			P162142.06.0325		P162142.06.0325
	66  			213 			P162142.06.0324		P162142.06.0324
	67  			212 			P162142.06.0323		P162142.06.0323
	68  			211 			P162142.06.0322		P162142.06.0322
	69  			210 			P162142.06.0321		P162142.06.0321
	70  			209 			P162142.06.0320		P162142.06.0320
	71  			208 			P162142.06.0319		P162142.06.0319
	72  			207 			P162142.06.0318		P162142.06.0318
	73  			206 			P162142.06.0317		P162142.06.0317
	74  			205 			P162142.06.0316		P162142.06.0316



PERFORMING SAMPLING PLAN FOR THE FOLLOWING 74   par_bins

 Updating part_bin: 1    		serial: P162142.06.0001 		from: 1 		to: 4
 Updating part_bin: 2    		serial: P162142.06.0002 		from: 1 		to: 4
 Updating part_bin: 3    		serial: P162142.06.0006 		from: 1 		to: 4
 Updating part_bin: 4    		serial: P162142.06.0016 		from: 1 		to: 4
 Updating part_bin: 5    		serial: P162142.06.0045 		from: 1 		to: 4
 Updating part_bin: 6    		serial: P162142.06.0046 		from: 1 		to: 4
 Updating part_bin: 7    		serial: P162142.06.0064 		from: 1 		to: 4
 Updating part_bin: 8    		serial: P162142.06.0083 		from: 1 		to: 4
 Updating part_bin: 9    		serial: P162142.06.0100 		from: 1 		to: 4
 Updating part_bin: 10   		serial: P162142.06.0101 		from: 1 		to: 4
 Updating part_bin: 11   		serial: P162142.06.0102 		from: 1 		to: 4
 Updating part_bin: 12   		serial: P162142.06.0103 		from: 1 		to: 4
 Updating part_bin: 13   		serial: P162142.06.0104 		from: 1 		to: 4
 Updating part_bin: 14   		serial: P162142.06.0105 		from: 1 		to: 4
 Updating part_bin: 15   		serial: P162142.06.0106 		from: 1 		to: 4
 Updating part_bin: 16   		serial: P162142.06.0107 		from: 1 		to: 4
 Updating part_bin: 17   		serial: P162142.06.0112 		from: 1 		to: 4
 Updating part_bin: 18   		serial: P162142.06.0113 		from: 1 		to: 4
 Updating part_bin: 19   		serial: P162142.06.0114 		from: 1 		to: 4
 Updating part_bin: 20   		serial: P162142.06.0115 		from: 1 		to: 4
 Updating part_bin: 21   		serial: P162142.06.0116 		from: 1 		to: 4
 Updating part_bin: 22   		serial: P162142.06.0117 		from: 1 		to: 4
 Updating part_bin: 23   		serial: P162142.06.0118 		from: 1 		to: 4
 Updating part_bin: 24   		serial: P162142.06.0122 		from: 1 		to: 4
 Updating part_bin: 25   		serial: P162142.06.0123 		from: 1 		to: 4
 Updating part_bin: 26   		serial: P162142.06.0124 		from: 1 		to: 4
 Updating part_bin: 27   		serial: P162142.06.0125 		from: 1 		to: 4
 Updating part_bin: 28   		serial: P162142.06.0127 		from: 1 		to: 4
 Updating part_bin: 29   		serial: P162142.06.0128 		from: 1 		to: 4
 Updating part_bin: 30   		serial: P162142.06.0129 		from: 1 		to: 4
 Updating part_bin: 31   		serial: P162142.06.0130 		from: 1 		to: 4
 Updating part_bin: 32   		serial: P162142.06.0131 		from: 1 		to: 4
 Updating part_bin: 33   		serial: P162142.06.0132 		from: 1 		to: 4
 Updating part_bin: 34   		serial: P162142.06.0133 		from: 1 		to: 4
 Updating part_bin: 35   		serial: P162142.06.0134 		from: 1 		to: 4
 Updating part_bin: 36   		serial: P162142.06.0135 		from: 1 		to: 4
 Updating part_bin: 205  		serial: P162142.06.0316 		from: 1 		to: 4
 Updating part_bin: 206  		serial: P162142.06.0317 		from: 1 		to: 4
 Updating part_bin: 207  		serial: P162142.06.0318 		from: 1 		to: 4
 Updating part_bin: 208  		serial: P162142.06.0319 		from: 1 		to: 4
 Updating part_bin: 209  		serial: P162142.06.0320 		from: 1 		to: 4
 Updating part_bin: 210  		serial: P162142.06.0321 		from: 1 		to: 4
 Updating part_bin: 211  		serial: P162142.06.0322 		from: 1 		to: 4
 Updating part_bin: 212  		serial: P162142.06.0323 		from: 1 		to: 4
 Updating part_bin: 213  		serial: P162142.06.0324 		from: 1 		to: 4
 Updating part_bin: 214  		serial: P162142.06.0325 		from: 1 		to: 4
 Updating part_bin: 215  		serial: P162142.06.0326 		from: 1 		to: 4
 Updating part_bin: 216  		serial: P162142.06.0327 		from: 1 		to: 4
 Updating part_bin: 217  		serial: P162142.06.0328 		from: 1 		to: 4
 Updating part_bin: 218  		serial: P162142.06.0329 		from: 1 		to: 4
 Updating part_bin: 219  		serial: P162142.06.0330 		from: 1 		to: 4
 Updating part_bin: 220  		serial: P162142.06.0331 		from: 1 		to: 4
 Updating part_bin: 221  		serial: P162142.06.0332 		from: 1 		to: 4
 Updating part_bin: 222  		serial: P162142.06.0333 		from: 1 		to: 4
 Updating part_bin: 223  		serial: P162142.06.0334 		from: 1 		to: 4
 Updating part_bin: 224  		serial: P162142.06.0335 		from: 1 		to: 4
 Updating part_bin: 225  		serial: P162142.06.0336 		from: 1 		to: 4
 Updating part_bin: 226  		serial: P162142.06.0337 		from: 1 		to: 4
 Updating part_bin: 227  		serial: P162142.06.0338 		from: 1 		to: 4
 Updating part_bin: 228  		serial: P162142.06.0339 		from: 1 		to: 4
 Updating part_bin: 229  		serial: P162142.06.0340 		from: 1 		to: 4
 Updating part_bin: 230  		serial: P162142.06.0341 		from: 1 		to: 4
 Updating part_bin: 231  		serial: P162142.06.0342 		from: 1 		to: 4
 Updating part_bin: 232  		serial: P162142.06.0343 		from: 1 		to: 4
 Updating part_bin: 233  		serial: P162142.06.0345 		from: 1 		to: 4
 Updating part_bin: 234  		serial: P162142.06.0346 		from: 1 		to: 4
 Updating part_bin: 235  		serial: P162142.06.0347 		from: 1 		to: 4
 Updating part_bin: 236  		serial: P162142.06.0348 		from: 1 		to: 4
 Updating part_bin: 237  		serial: P162142.06.0349 		from: 1 		to: 4
 Updating part_bin: 238  		serial: P162142.06.0350 		from: 1 		to: 4
 Updating part_bin: 239  		serial: P162142.06.0351 		from: 1 		to: 4
 Updating part_bin: 240  		serial: P162142.06.0352 		from: 1 		to: 4
 Updating part_bin: 241  		serial: P162142.06.0894 		from: 1 		to: 4
 Updating part_bin: 242  		serial: P162142.06.0895 		from: 1 		to: 4
	SAMPLING PLAN: COMPLETE

	SAMPLING PLAN: USED REPLACEMENTS


	SAMPLING PLAN DONE