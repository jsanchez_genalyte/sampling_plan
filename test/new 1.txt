
 Initializing Sampling Plan Rev 1.00   ...

 DONE Reading Input Parameters file:

 sp_master_excel_file_name   = C:\Users\jsanchez\Documents\sampling_plan\configure\sampling_plan_master_official_nick_test_1.xlsm
 sampling_plan_number_to_use = 2
 wo_number                   = DWO-0051
 seq_num                     = 40
 op_code                     = D320
 op_code_previous_step       = A230
 bin_number_from             = 1
 bin_number_to               = 2
 check_availab_only_flag     = false
 sp_ok_do_partially_flag     = true
 replace_chip_if_miss_flag   = true
 DONE                      ... reading CONFIG PARAMETERS

 Reading ... Sampling plan Master File (excel):    C:\Users\jsanchez\Documents\sampling_plan\configure\sampling_plan_master_official_nick_test_1.xlsm

 DONE  ... reading Sampling Plans:         2   sampling plans
Retrieving document at 'http://10.0.2.226/runcard_test/soap?wsdl'


 ok

 Found SP indices: From  0    To 726  Larger than the Available WO indices  168
done with first pass

 WORK ORDER CHIP SUMMARY:
    Total into spotting      chip count       168
    Available good           chip count       166
    Failures (Visual Insp)   chip count       2

 SAMPLING PLAN REQUEST FLAGS SUMMARY:
    Check Availability Only (just check) FLAG FALSE
    Sampling_plan_ok_do_partial          FLAG TRUE
    Replace chip if is missing           FLAG TRUE

 SAMPLING PLAN DETAILS
   Sampling plan name   to use:               Sampling_plan 2
   Sampling plan number to use:               2
   Requested            SP chip count         74

 SAMPLING PLAN SUMMARY RESULTS
 Sampling plan can be done completely:        TRUE
   Filled              SP chip count          74
   Missing             SP chip count          0

 SAMPLING PLAN DETAIL RESULTS:

   REQUESTED SAMPLING PLAN Die index
                1               2               3               4               5
                6               7               8               9               10
                11              12              13              14              15
                16              17              18              19              20
                21              22              23              24              25
                26              27              28              29              30
                31              32              33              34              35
                36              359             369             379             389
                399             409             419             429             439
                449             459             468             478             488
                498             508             518             527             538
                548             558             568             578             587
                597             607             617             627             637
                647             657             667             677             686
                697             706             716             726

   FAILURE Die index            Serial

                        1                       P162142.06.0001
                        2                       P162142.06.0130

   REPLACEMENT DIE OK:
        index_fail      index_with      serial_fail             serial_replace
        1                       37                      P162142.06.0001         P162142.06.0174
        2                       38                      P162142.06.0130         P162142.06.0175

   REPLACEMENT DIE MISSING: None (All replacement needed where done fine)

   Filled    BEYOND    WORK ORDER chip count          38
   Missing   BEYOND    WORK ORDER chip count          0
   REPLACEMENT DIE OK:    BEYOND AVAILABLE
        index_from      		index_with      		serial_from             serial_replace BEYOND AVAILABLE
        37                      168                     P162142.06.0315         P162142.06.0315
        38                      167                     P162142.06.0313         P162142.06.0313
        39                      166                     P162142.06.0312         P162142.06.0312
        40                      165                     P162142.06.0311         P162142.06.0311		
        41                      164                     P162142.06.0310         P162142.06.0310
        42                      163                     P162142.06.0309         P162142.06.0309
        43                      162                     P162142.06.0308         P162142.06.0308
        44                      161                     P162142.06.0307         P162142.06.0307
        45                      160                     P162142.06.0306         P162142.06.0306
        46                      159                     P162142.06.0305         P162142.06.0305
        47                      158                     P162142.06.0304         P162142.06.0304
        48                      157                     P162142.06.0303         P162142.06.0303
        49                      156                     P162142.06.0302         P162142.06.0302
        50                      155                     P162142.06.0301         P162142.06.0301
        51                      154                     P162142.06.0300         P162142.06.0300
        52                      153                     P162142.06.0299         P162142.06.0299
        53                      152                     P162142.06.0298         P162142.06.0298
        54                      151                     P162142.06.0297         P162142.06.0297
        55                      150                     P162142.06.0296         P162142.06.0296
        56                      149                     P162142.06.0295         P162142.06.0295
        57                      148                     P162142.06.0294         P162142.06.0294
        58                      147                     P162142.06.0293         P162142.06.0293
        59                      146                     P162142.06.0292         P162142.06.0292
        60                      145                     P162142.06.0291         P162142.06.0291
        61                      144                     P162142.06.0290         P162142.06.0290
        62                      143                     P162142.06.0289         P162142.06.0289
        63                      142                     P162142.06.0288         P162142.06.0288
        64                      141                     P162142.06.0287         P162142.06.0287
        65                      140                     P162142.06.0286         P162142.06.0286
        66                      139                     P162142.06.0285         P162142.06.0285
        67                      138                     P162142.06.0284         P162142.06.0284
        68                      137                     P162142.06.0283         P162142.06.0283
        69                      136                     P162142.06.0282         P162142.06.0282
        70                      135                     P162142.06.0281         P162142.06.0281
        71                      134                     P162142.06.0279         P162142.06.0279
        72                      133                     P162142.06.0278         P162142.06.0278
        73                      132                     P162142.06.0277         P162142.06.0277
        74                      131                     P162142.06.0276         P162142.06.0276



PERFORMING SAMPLING PLAN FOR THE FOLLOWING 74   par_bins

 Updating part_bin: 3                   serial: P162142.06.0138                 from: 1                 to: 2
 ERROR_WHILE PERFOMING SAMPLING PLAN: RunCard exception encountered (updateUnitInventoryPartBin): 
 Warehouse bin is not valid, expecting FINISHED GOODS or INVENTORY for this operation

ERROR:,
Warehouse bin is not valid, expecting FINISHED GOODS or INVENTORY for this operation\nError_value =1

        SAMPLING PLAN DONE

 To produce a TEXT report of this output:

 To Copy to the clipoard all the output do: Edit_menu --> Select All --> Enter

 Then Paste into an email message: CTRL-V to share the report with the Sampling Plan Requester. ...

 Bye ...
